﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Barolo {
	namespace Events {

		public class EventManager : MonoBehaviour {

			private Dictionary<EventName, Action<GameEventArgs>> _eventDictionary;
			private Queue<EventQueueInfo> _eventQueue;

			private static EventManager _instance;
			public static EventManager Instance {
				get {
					if (!_instance) {
						_instance = FindObjectOfType(typeof(EventManager)) as EventManager;
						if (_instance != null) {
							_instance.Init();
						}
					}
					return _instance;
				}
			}

			private struct EventQueueInfo {
				public EventName eventName;
				public GameEventArgs args;

				public EventQueueInfo(EventName name, GameEventArgs args) {
					this.eventName = name;
					this.args = args;
				}
			}

			void Init() {
				if (_eventDictionary == null) {
					_eventDictionary = new Dictionary<EventName, Action<GameEventArgs>>();
				}
				if (_eventQueue == null) {
					_eventQueue = new Queue<EventQueueInfo>();
				}
			}

			public static void StartListening(EventName eventType, Action<GameEventArgs> listener) {
				if (Instance == null) {
					return;
				}

				Action<GameEventArgs> thisEvent;
				if (Instance._eventDictionary.TryGetValue(eventType, out thisEvent)) {
					thisEvent += listener;
					Instance._eventDictionary[eventType] = thisEvent;
				} else {
					thisEvent += listener;
					Instance._eventDictionary.Add(eventType, thisEvent);
				}
			}

			public static void StopListening(EventName eventType, Action<GameEventArgs> listener) {
				if (Instance == null) {
					return;
				}

				Action<GameEventArgs> thisEvent;
				if (Instance._eventDictionary.TryGetValue(eventType, out thisEvent)) {
					thisEvent -= listener;
				}
			}

			public static void AddToQueue(EventName eventName, GameEventArgs args) {
				if (Instance == null) {
					return;
				}

				EventQueueInfo queueInfo = new EventQueueInfo(eventName, args);
				if (Instance._eventQueue.Count == 0) {
					TriggerEvent(queueInfo);
				} else {
					Instance._eventQueue.Enqueue(queueInfo);
				}
			}

			private static void TriggerEvent(EventQueueInfo info) {
				if (Instance == null) {
					return;
				}

				Action<GameEventArgs> thisEvent;
				if (Instance._eventDictionary.TryGetValue(info.eventName, out thisEvent)) {
					thisEvent.Invoke(info.args);
				}
				if (Instance._eventQueue.Count > 0) {
					TriggerEvent(Instance._eventQueue.Dequeue());
				}
			}
		}

	}
}
