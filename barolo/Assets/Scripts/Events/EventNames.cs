﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

namespace Barolo {
	namespace Events {

		public enum EventName {

			#region UI Interaction

			CLICK,
			BUTTON_CLICK,

			#endregion

			#region Encounter

			END_ENCOUNTER,
			DREW_CARD,

			#region Major Encounter

			ACTIVATE_ABILITY,
			DECK_SETUP,
			SPAWN_ENEMY,
			ENEMY_DEATH,
			REFRESH_RESOURCES,

			#endregion

			#region Minor Encounter

			NODE_ADVANCED,
			DIALOGUE_ENCOUNTERED,
			CHOICES_ENCOUNTERED,
			CHOICE_SELECTED,
			SKILL_CHECK_ENCOUNTERED,
			SKILL_CHECK_COMPLETE,

			#endregion

			#region Ability Events

			DAMAGE_DEALT,
			CHANGE_ZONE,
			CHOOSING_TARGET,
			TARGET_CHOSEN,
			ALL_TARGETS_CHOSEN,
			PAYING_COST,
			COST_PAID,
			ALL_COSTS_PAID,

			#endregion

			#endregion
		}

	}
}
