﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Encounter.UI;

	namespace Events {

		public class ButtonEventArgs : GameEventArgs {

			public ButtonEventArgs(ButtonType buttonType) {
				this.buttonType = buttonType;
			}

			public ButtonType buttonType { get; private set; }

		}

	}
}
