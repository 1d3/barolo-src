﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace Events {

		public class ClickEventArgs : GameEventArgs {

			public ClickEventArgs(IInteractableGameActor target) {
				this.target = target;
			}

			public IInteractableGameActor target { get; private set; }

		}

	}
}
