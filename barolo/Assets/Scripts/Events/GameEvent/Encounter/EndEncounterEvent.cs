﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Events {

		public class EndEncounterEventArgs : GameEventArgs {

			public EndEncounterEventArgs(bool playerDefeated) {
				this.playerDefeated = playerDefeated;
			}

			public bool playerDefeated { get; private set; }

		}

	}
}
