﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace Events {

		public class DrawCardsEventArgs : GameEventArgs {

			public DrawCardsEventArgs(int amount) {
				this.amount = amount;
			}

			public int amount { get; private set; }
		}

	}
}
