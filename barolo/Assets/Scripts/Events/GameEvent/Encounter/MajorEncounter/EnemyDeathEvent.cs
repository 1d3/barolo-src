﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {

	using Encounter.Enemies;

	namespace Events {

		public class EnemyDeathEventArgs : GameEventArgs {

			public EnemyDeathEventArgs(Enemy enemy) {
				this.enemy = enemy;
			}

			public Enemy enemy { get; private set; }

		}

	}
}
