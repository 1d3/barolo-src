﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {

	using Cards;

	namespace Events {

		public class DeckSetupEventArgs : GameEventArgs {

			public DeckSetupEventArgs(Deck deck) {
				this.deck = deck;
			}

			public Deck deck { get; private set; }

		}

	}
}
