﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Events {

		public class DamageDealtEventArgs : GameEventArgs {

			public DamageDealtEventArgs(int amount, DamageType type, IInteractableGameActor source, IInteractableGameActor target, int remainingHealth) {
				this.amount = amount;
				this.type = type;
				this.source = source;
				this.target = target;
				this.remainingHealth = remainingHealth;

			}

			public int amount { get; private set; }

			public DamageType type { get; private set; }

			public IInteractableGameActor source { get; private set; }

			public IInteractableGameActor target { get; private set; }

			public int remainingHealth { get; private set; }
		}

	}
}
