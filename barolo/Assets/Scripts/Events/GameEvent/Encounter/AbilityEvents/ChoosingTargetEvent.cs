﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {

	using Abilities;

	namespace Events {

		public class ChoosingTargetEventArgs : GameEventArgs {

			public ChoosingTargetEventArgs(Effect effect) {
				this.effect = effect;
			}

			public Effect effect { get; private set; }

		}

	}
}
