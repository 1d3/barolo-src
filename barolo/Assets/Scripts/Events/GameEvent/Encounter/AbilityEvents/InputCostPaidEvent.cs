﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Abilities;

	namespace Events {

		public class InputCostPaidEventArgs : GameEventArgs {

			public InputCostPaidEventArgs(Cost cost) {
				this.cost = cost;
			}

			public Cost cost { get; private set; }

		}

	}
}
