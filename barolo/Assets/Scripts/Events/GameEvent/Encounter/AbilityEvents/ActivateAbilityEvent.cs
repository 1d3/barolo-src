﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Abilities;

	namespace Events {

		public class ActivateAbilityEventArgs : GameEventArgs {

			public ActivateAbilityEventArgs(IInteractableGameActor source, ActivatedAbility ability) {
				this.source = source;
				this.ability = ability;
			}

			public IInteractableGameActor source { get; private set; }

			public ActivatedAbility ability { get; private set; }

		}

	}
}
