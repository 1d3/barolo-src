﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Abilities;

	namespace Events {

		public class PayingInputCostEventArgs : GameEventArgs {

			public PayingInputCostEventArgs(Cost cost) {
				this.cost = cost;
			}

			public Cost cost { get; private set; }

		}

	}
}
