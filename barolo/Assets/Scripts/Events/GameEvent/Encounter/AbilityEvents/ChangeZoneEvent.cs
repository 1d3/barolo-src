﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace Events {

		public class ChangeZoneEventArgs : GameEventArgs {

			public ChangeZoneEventArgs(IInteractableGameActor source, ZoneType start, ZoneType end) {
				this.source = source;
				this.start = start;
				this.end = end;
			}

			public IInteractableGameActor source { get; private set; }
			public ZoneType start { get; private set; }
			public ZoneType end { get; private set; }

		}

	}
}
