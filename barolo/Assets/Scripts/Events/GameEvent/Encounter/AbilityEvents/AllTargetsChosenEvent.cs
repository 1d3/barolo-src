﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {

	using Abilities;

	namespace Events {

		public class AllTargetsChosenEventArgs : GameEventArgs {

			public AllTargetsChosenEventArgs(IInteractableGameActor source, Dictionary<Effect, HashSet<IInteractableGameActor>> effectsToTargets) {
				this.source = source;
				this.effectsToTargets = effectsToTargets;
			}

			public IInteractableGameActor source { get; private set; }

			public Dictionary<Effect, HashSet<IInteractableGameActor>> effectsToTargets { get; private set; }

		}

	}
}
