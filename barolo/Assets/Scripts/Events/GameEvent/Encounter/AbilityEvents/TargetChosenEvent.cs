﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {

	using Abilities;

	namespace Events {

		public class TargetChosenEventArgs : GameEventArgs {

			public TargetChosenEventArgs(IInteractableGameActor source, Effect effect, IInteractableGameActor target) {
				this.source = source;
				this.effect = effect;
				this.target = target;
			}

			public IInteractableGameActor source { get; private set; }

			public Effect effect { get; private set; }

			public IInteractableGameActor target { get; private set; }

		}

	}
}
