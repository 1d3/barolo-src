﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace Events {

		public class DialogueEventArgs : GameEventArgs {
			
			public DialogueEventArgs(string line) {
				this.line = line;
			}

			public string line { get; private set; }
		}

	}
}
