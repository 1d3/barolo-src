﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace Events {

		public class ChoicesEventArgs : GameEventArgs {

			public ChoicesEventArgs(string[] choiceKeys) {
				this.choiceKeys = choiceKeys;
			}

			public string[] choiceKeys { get; private set; }
		}

	}
}
