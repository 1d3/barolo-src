﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace Events {

		public class SkillCheckCompleteEventArgs : GameEventArgs {

			public SkillCheckCompleteEventArgs(int checkValue) {
				this.checkValue = checkValue;
			}

			public int checkValue { get; private set; }
		}

	}
}
