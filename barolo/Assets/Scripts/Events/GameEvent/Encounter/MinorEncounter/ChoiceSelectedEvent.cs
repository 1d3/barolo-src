﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace Events {

		public class ChoiceSelectedEventArgs : GameEventArgs {
			
			public ChoiceSelectedEventArgs(string key) {
				this.choiceKey = key;
			}

			public string choiceKey { get; private set; }
		}

	}
}
