﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {
	using Cards;

	namespace Events {

		public class PlayerDrewCardEventArgs : GameEventArgs {


			public PlayerDrewCardEventArgs(Card card, int libraryCount) {
				this.drawnCard = card;
				this.libraryCount = libraryCount;
			}

			public Card drawnCard { get; private set; }

			public int libraryCount { get; private set; }
		}

	}
}
