﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;
using System.Collections.Generic;

namespace Barolo {

	namespace Abilities {

		[Serializable]
		public class ActivatedAbility : Ability {

			public Cost[] costs;

		}

	}
}
