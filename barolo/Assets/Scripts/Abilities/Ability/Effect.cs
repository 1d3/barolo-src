﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;
using System.Collections.Generic;

namespace Barolo {
	namespace Abilities {

		[Serializable]
		public class Effect {

			public AbilityEffect[] abilityEffects;
			public Target target;
			public ZoneType[] targetZones;

			public void Resolve(IInteractableGameActor source, HashSet<IInteractableGameActor> targets) {
				foreach(AbilityEffect effect in abilityEffects) {
					effect.Resolve(source, targets);
				}
			}

			public HashSet<IInteractableGameActor> GetDefaultTargets() {
				TargetQuantity targetQuantity = this.target.Quantity();
				HashSet<IInteractableGameActor> initialTargetList = new HashSet<IInteractableGameActor>();

				switch (targetQuantity) {
					case TargetQuantity.ALL:
						GameObject[] gos = GameObject.FindGameObjectsWithTag(this.target.TargetTag());
						foreach (GameObject go in gos) {
							initialTargetList.Add(go.GetComponent<IInteractableGameActor>());
						}
						break;
					case TargetQuantity.NONE:
						initialTargetList = null;
						break;
					default:
						break;
				}

				return initialTargetList;
			}

		}

	}
}
