﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;

namespace Barolo {

	using Encounter;

	namespace Abilities {

		[Serializable]
		public abstract class Ability {

			public ZoneType[] validZones;

			public Effect[] effects;

			public bool CanAct(IInteractableGameActor source) {
				bool inValidZone = Array.IndexOf(validZones, source.CurrentZone) > -1;
				return inValidZone && OtherActivationRestrictions(source);
			}

			protected virtual bool OtherActivationRestrictions(IInteractableGameActor source) {
				return true;
			}

		}

	}
}
