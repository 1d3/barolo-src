﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System;
using UnityEngine;

namespace Barolo {
	namespace Abilities {

		[Serializable]
		public class TriggeredAbility : Ability {

			public Condition condition;

			protected override bool OtherActivationRestrictions(IInteractableGameActor source) {
				return condition.FulfillsCondition(source);
			}

			public void SetListening(bool active) {
				if (active) {
					condition.StartListening();
				} else {
					condition.StopListening();
				}
			}

		}

	}
}
