﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {

	using Events;
	using Encounter.MajorEncounter;

	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Costs/DiscardCost")]
		public class DiscardCost : PlayerInputCost {

			public Number number;

			public override void UndoPayWith(IInteractableGameActor actor) {
				ChangeZoneEventArgs args = new ChangeZoneEventArgs(actor, ZoneType.DISCARD, ZoneType.HAND);
				EventManager.AddToQueue(EventName.CHANGE_ZONE, args);
			}

			public override bool PayWith(IInteractableGameActor actor) {
				if (actor.CurrentZone == ZoneType.HAND) {
					ChangeZoneEventArgs args = new ChangeZoneEventArgs(actor, ZoneType.HAND, ZoneType.DISCARD);
					EventManager.AddToQueue(EventName.CHANGE_ZONE, args);
					return true;
				}
				return false;
			}

			public override bool ActorsCompletePay(IInteractableGameActor source, HashSet<IInteractableGameActor> actors) {
				return actors.Count == number.Calculate(source);
			}

		}

	}
}
