﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {

	using Encounter.Resource;

	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Costs/ActionPointCost")]
		public class ActionPointCost : AutomatedCost {

			public Number num;

			public override bool AutomatedPay(IInteractableGameActor actor) {
				ActionPoints ap = playerActionPoints();
				if (ap == null) {
					return false;
				}
				int calcedNumber = num.Calculate(actor);
				if (ap.current < calcedNumber) {
					return false;
				}
				ap.Pay(calcedNumber);
				return true;
			}

			public override void UndoPay(IInteractableGameActor actor) {
				playerActionPoints().Add(num.Calculate(actor));
			}

			protected ActionPoints playerActionPoints() {
				return GameObject.FindGameObjectWithTag(Constants.Tags.PLAYER_TAG).GetComponent<ActionPoints>();
			}

		}

	}
}
