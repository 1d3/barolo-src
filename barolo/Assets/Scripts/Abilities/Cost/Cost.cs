﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System.Collections.Generic;
using UnityEngine;

namespace Barolo {

	namespace Abilities {

		public abstract class Cost : ScriptableObject {

			public abstract bool AutomatedPay(IInteractableGameActor actor);

			public virtual bool PayWith(IInteractableGameActor actor) {
				return false;
			}

			public virtual void UndoPay(IInteractableGameActor actor) {}

			public virtual void UndoPayWith(IInteractableGameActor actor) {}

			public virtual bool ActorsCompletePay(IInteractableGameActor source, HashSet<IInteractableGameActor> actors) {
				return false;
			}

			[HideInInspector] public bool canAutomate;

		}

	}
}
