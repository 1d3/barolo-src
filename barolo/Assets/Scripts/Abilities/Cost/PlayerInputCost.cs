﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {

	namespace Abilities {

		public abstract class PlayerInputCost : Cost {

			void OnValidate() {
				canAutomate = false;
			}

			public override bool AutomatedPay(IInteractableGameActor actor) {
				return false;
			}

		}

	}
}
