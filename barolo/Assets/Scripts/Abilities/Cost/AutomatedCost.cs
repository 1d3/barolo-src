﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	namespace Abilities {

		public abstract class AutomatedCost : Cost {

			void OnValidate() {
				canAutomate = true;
			}

		}

	}
}
