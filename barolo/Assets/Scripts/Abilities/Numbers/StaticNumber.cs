﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Numbers/Static")]
		public class StaticNumber : Number {

			public int number;

			public override int Calculate(IInteractableGameActor source) {
				return number;
			}

		}

	}
}
