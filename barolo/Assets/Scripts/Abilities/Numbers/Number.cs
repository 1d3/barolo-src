﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Abilities {

		public abstract class Number : ScriptableObject {

			public abstract int Calculate(IInteractableGameActor source);

		}

	}
}
