﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Encounter.Resource;

	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Numbers/Resource")]
		public class ResourceNumber : Number {

			public ResourceType resourceType;

			public override int Calculate(IInteractableGameActor source) {
				BaseResource resource = source.ActorGameObject.GetComponent(ResourceManager.resources[resourceType]) as BaseResource;
				return resource == null ? 0 : resource.current;
			}

		}

	}
}
