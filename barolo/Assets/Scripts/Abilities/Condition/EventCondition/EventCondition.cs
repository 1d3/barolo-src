﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Abilities {

		public abstract class EventCondition : Condition {

			protected bool eventTriggered = false;

			public override bool FulfillsCondition(IInteractableGameActor source) {
				return eventTriggered;
			}

			public override void ResetState() {
				eventTriggered = false;
			}

		}

	}
}
