﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Events;

	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Conditions/Discard Condition")]
		public class DiscardCondition : EventCondition {

			public override void StartListening() {
				EventManager.StartListening(EventName.CHANGE_ZONE, OnChangeZone);
			}

			public override void StopListening() {
				EventManager.StopListening(EventName.CHANGE_ZONE, OnChangeZone);
			}

			void OnChangeZone(GameEventArgs args) {
				ChangeZoneEventArgs changeArgs = (ChangeZoneEventArgs)args;

				if (changeArgs.start == ZoneType.HAND && changeArgs.end == ZoneType.DISCARD) {
					eventTriggered = true;
				}
			}

		}

	}
}
