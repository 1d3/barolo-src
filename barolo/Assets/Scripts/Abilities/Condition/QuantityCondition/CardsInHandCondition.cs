﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Conditions/CardsInHandCondition")]
		public class CardsInHandCondition : QuantityCondition {

			public Number number;

			public override bool FulfillsCondition(IInteractableGameActor source) {
				Player player = source.ActorGameObject.GetComponent<Player>();
				
				if (player == null) {
					return false;
				}

				return Compare(player.hand.Count, number.Calculate(source));
				 
			}

		}

	}
}
