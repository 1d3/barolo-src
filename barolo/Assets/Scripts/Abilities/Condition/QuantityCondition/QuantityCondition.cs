﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Abilities {

		public abstract class QuantityCondition : Condition {

			public bool equals;
			public bool greaterThan;
			public bool lessThan;

			void OnValidate() {
				if (greaterThan && lessThan) {
					Debug.LogError("QuantityCondition has both greaterThan and lessThan, setting both to false");
					lessThan = false;
					greaterThan = false;
				}
			}

			public bool Compare(int current, int given) {
				if (current == given) {
					return equals;
				} else if (current > given) {
					return greaterThan;
				}

				return lessThan;
			}

		}

	}
}
