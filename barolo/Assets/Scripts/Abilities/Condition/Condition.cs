﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System;
using UnityEngine;

namespace Barolo {
	namespace Abilities {

		public abstract class Condition : ScriptableObject {

			public abstract bool FulfillsCondition(IInteractableGameActor source);

			public virtual void ResetState() {}

			public virtual void StartListening() {}
			public virtual void StopListening() {}

		}

	}
}
