﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;
using System.Collections.Generic;

namespace Barolo {
	using Events;

	namespace Abilities {

		public abstract class AbilityEffect : ScriptableObject {

			public void Resolve(IInteractableGameActor source, HashSet<IInteractableGameActor> targets) {
				HandleResolve(source, targets);
			}

			public virtual int EvalDamage(IInteractableGameActor source) {
				return 0;
			}

			protected abstract void HandleResolve(IInteractableGameActor source, HashSet<IInteractableGameActor> targets);

		}

	}
}
