﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {

	using Encounter.Resource;

	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Effects/Modify Resource")]
		public class ModifyResourceEffect : AbilityEffect {

			public Number number;
			public ResourceType resourceType;

			protected override void HandleResolve(IInteractableGameActor source, HashSet<IInteractableGameActor> targets) {
				BaseResource resource = source.ActorGameObject.GetComponent(ResourceManager.resources[resourceType]) as BaseResource;
				if (resource != null) {
					resource.Add(number.Calculate(source));
				}
			}

		}

	}
}
