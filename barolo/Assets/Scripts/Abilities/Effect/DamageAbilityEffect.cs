﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {
	using Events;

	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Effects/Damage Ability")]
		public class DamageAbilityEffect : AbilityEffect {

			public Number number;
			public DamageType damageType;

			public override int EvalDamage(IInteractableGameActor source) {
				return number.Calculate(source);
			}

			protected override void HandleResolve(IInteractableGameActor source, HashSet<IInteractableGameActor> targets) {
				foreach (IInteractableGameActor target in targets) {
					target.DealDamage(source, damageType, number.Calculate(source));
				}
			}

		}

	}
}
