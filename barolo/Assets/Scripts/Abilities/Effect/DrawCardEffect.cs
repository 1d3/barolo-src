﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {
	using Events;

	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Effects/Draw Card")]
		public class DrawCardEffect : AbilityEffect {

			public Number num;

			protected override void HandleResolve(IInteractableGameActor source, HashSet<IInteractableGameActor> targets) {
				Player player = GameObject.FindGameObjectWithTag(Constants.Tags.PLAYER_TAG).GetComponent<Player>();
				player.DrawCards(num.Calculate(source));
			}

		}

	}
}
