﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {
	using Events;

	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Effects/Change Self Zone")]
		public class ChangeSelfZoneEffect : AbilityEffect {

			public ZoneType destination;

			protected override void HandleResolve(IInteractableGameActor source, HashSet<IInteractableGameActor> targets) {
				ChangeZoneEventArgs args = new ChangeZoneEventArgs(source, source.CurrentZone, destination);
				EventManager.AddToQueue(EventName.CHANGE_ZONE, args);
			}

		}

	}
}
