﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Target/ManyTarget")]
		public class ManyTarget : Target {

			public int min;
			public int max;

			public override TargetQuantity Quantity() {
				return TargetQuantity.MANY;
			}

			public override int Max() {
				return max;
			}

			public override int Min() {
				return min;
			}

		}

	}
}
