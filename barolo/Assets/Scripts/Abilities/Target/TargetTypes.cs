﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace Abilities {

		public enum TargetType {
			NONE,
			PLAYER,
			ARTIFACT,
			ENEMY
		}

	}
}
