﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;
using System.Collections.Generic;

namespace Barolo {

	namespace Abilities {

		public abstract class Target : ScriptableObject {

			public TargetType targetType;
			
			public string TargetTag() {
				switch (targetType) {
					case TargetType.PLAYER:
						return Constants.Tags.PLAYER_TAG;
					case TargetType.ENEMY:
						return Constants.Tags.ENEMY_TAG;
					case TargetType.ARTIFACT:
						return Constants.Tags.CARD_TAG;
					default:
						Debug.LogError(String.Format("Cannot convert target type {0} to tag name", targetType));
						return default(string);
				}
			}

			public abstract int Min();
			public abstract int Max();
			public abstract TargetQuantity Quantity();

		}

	}
}
