﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Target/NoneTarget")]
		public class NoneTarget : Target {

			public override TargetQuantity Quantity() {
				return TargetQuantity.NONE;
			}

			public override int Max() {
				return 0;
			}

			public override int Min() {
				return 0;
			}

		}

	}
}
