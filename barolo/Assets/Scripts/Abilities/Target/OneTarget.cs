﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Target/OneTarget")]
		public class OneTarget : Target {

			public override TargetQuantity Quantity() {
				return TargetQuantity.ONE;
			}

			public override int Max() {
				return 1;
			}

			public override int Min() {
				return 1;
			}

		}

	}
}
