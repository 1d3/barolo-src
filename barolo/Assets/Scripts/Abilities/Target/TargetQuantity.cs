﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Abilities {

		public enum TargetQuantity {
			NONE,
			ONE,
			MANY,
			ALL
		}

	}
}
