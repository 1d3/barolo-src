﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Abilities {

		[CreateAssetMenu(menuName="Abilities/Target/AllTarget")]
		public class AllTarget : Target {

			public override TargetQuantity Quantity() {
				return TargetQuantity.ALL;
			}

			public override int Max() {
				return int.MaxValue;
			}

			public override int Min() {
				return int.MaxValue;
			}

		}

	}
}
