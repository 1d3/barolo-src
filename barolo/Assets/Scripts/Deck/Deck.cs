﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {
	namespace Cards {

		public class Deck {

			private Stack<Card> library = new Stack<Card>();

			public int libraryCount {
				get {
					return library.Count;
				}
			}

			public Deck(Card[] cards) {
				foreach (Card card in cards) {
					library.Push(card);
				}
			}

			public void Shuffle() {
				Card[] values = library.ToArray();
				library.Clear();
				for (int i = 0; i < values.Length; i++) {
					Card temp = values[i];
					int randomIndex = Random.Range(i, values.Length);
					values[i] = values[randomIndex];
					values[randomIndex] = temp;
				}
				foreach (Card value in values) {
					library.Push(value);
				}
			}

			public Card[] GetCards() {
				return library.ToArray();
			}

			public Card DrawCard() {
				return libraryCount > 0 ? library.Pop() : null;
			}
		}

	}
}
