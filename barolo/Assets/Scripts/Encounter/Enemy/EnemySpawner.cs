﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System;
using System.IO;
using UnityEngine;

namespace Barolo {

	using Events;
	using SaveLoad.Bundles;
	using Encounter.MajorEncounter;

	namespace Encounter.Enemies {

		public class EnemySpawner : MonoBehaviour {

			void OnEnable() {
				EventManager.StartListening(EventName.SPAWN_ENEMY, OnSpawnEnemy);
			}

			void OnDisable() {
				EventManager.StopListening(EventName.SPAWN_ENEMY, OnSpawnEnemy);
			}

			void OnSpawnEnemy(GameEventArgs args) {
				for (int i = 0; i < 3; i++) {
					Enemy enemy = EnemyLoader.LoadEnemy("Void-Gang Thug", Constants.Bundles.EnemiesBundle);
					GameObject enemyObject = Instantiate(enemy.gameObject, this.gameObject.transform);
					enemyObject.GetComponent<Enemy>().ChangeZone(ZoneType.BATTLEFIELD);
				}
			}

		}

	}
}
