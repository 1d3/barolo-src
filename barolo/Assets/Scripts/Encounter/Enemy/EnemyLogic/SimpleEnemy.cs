﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Barolo {

	using Abilities;
	using AI;
	using Encounter.MajorEncounter;
	using Encounter.Resource;

	namespace Encounter.Enemies {
	
		public class SimpleEnemy : Enemy {

			protected override Dictionary<Effect, HashSet<IInteractableGameActor>> DetermineActions(CommandType command) {
				if (data.abilities.Length == 0) {
					Debug.LogError(String.Format("Enemy {0} has no abilities", this.name));
				}
				ActivatedAbility abilityToUse = data.abilities[0];
				switch (command) {
					default:
						abilityToUse = LargeDamage();
						break;
				}
				return ChooseTargets(abilityToUse);

			}

			protected ActivatedAbility LargeDamage() {
				int strongestAttack = -1;
				ActivatedAbility strongestAbility = data.abilities[0];
				foreach (ActivatedAbility ability in data.abilities) {
					if (!ability.CanAct(this)) {
						continue;
					}
					int abilityDamage = 0;
					foreach (Effect effect in ability.effects) {
						foreach (AbilityEffect abilityEffect in effect.abilityEffects) {
							int damage = abilityEffect.EvalDamage(this);
							GameObject[] affectedActors = GameObject.FindGameObjectsWithTag(effect.target.TargetTag());
							affectedActors = affectedActors.Where(actor => Array.Exists(effect.targetZones, element => element == actor.GetComponent<IInteractableGameActor>().CurrentZone)).ToArray();
							affectedActors = affectedActors.Where(actor => actor.GetComponent<IInteractableGameActor>().GetResource<Health>().current > 0).ToArray();
							damage = damage * Math.Min(effect.target.Max(), affectedActors.Length);
							abilityDamage += damage;
						}
					}
					if (abilityDamage > strongestAttack) {
						strongestAttack = abilityDamage;
						strongestAbility = ability;
					}
				}

				return strongestAbility;
			}

			protected Dictionary<Effect, HashSet<IInteractableGameActor>> ChooseTargets(ActivatedAbility ability) {
				Dictionary<Effect, HashSet<IInteractableGameActor>> effectsToTargets = new Dictionary<Effect, HashSet<IInteractableGameActor>>();

				Dictionary<TargetType, List<IInteractableGameActor>> actors = new Dictionary<TargetType, List<IInteractableGameActor>>();
				Dictionary<IInteractableGameActor, int> healthTracker = new Dictionary<IInteractableGameActor, int>();				

				List<Effect> unresolvedEffects = new List<Effect>();

				foreach (Effect effect in ability.effects) {
					if (!actors.ContainsKey(effect.target.targetType)) {
						GameObject[] actorGos = GameObject.FindGameObjectsWithTag(effect.target.TargetTag());
						List<IInteractableGameActor> actorComponents = new List<IInteractableGameActor>();
						foreach (GameObject actorGo in actorGos) {
							IInteractableGameActor actor = actorGo.GetComponent<IInteractableGameActor>();
							if (Array.Exists(effect.targetZones, element => element == actor.CurrentZone)) {
								actorComponents.Add(actor);
								healthTracker.Add(actor, actor.GetResource<Health>().current);
							}
						}
						actors[effect.target.targetType] = actorComponents;
					}

					if (effect.target.Quantity() == TargetQuantity.ALL) {
						effectsToTargets[effect] = new HashSet<IInteractableGameActor>(actors[effect.target.targetType]);
						foreach (IInteractableGameActor actor in actors[effect.target.targetType]) {
							int damageSum = effect.abilityEffects.Sum(item => item.EvalDamage(this));
							healthTracker[actor] -= damageSum;
						}
					} else if (effect.target.Quantity() == TargetQuantity.NONE) {
						effectsToTargets[effect] = null;
					} else {
						effectsToTargets[effect] = new HashSet<IInteractableGameActor>();
						unresolvedEffects.Add(effect);
					}
				}

				foreach (Effect effect in unresolvedEffects) {
					if (effect.target.Quantity() == TargetQuantity.ONE) {
						int damageSum = effect.abilityEffects.Sum(item => item.EvalDamage(this));
						int closest = int.MaxValue;
						List<IInteractableGameActor> potentialTargets = actors[effect.target.targetType];
						if (potentialTargets.Count == 0) {
							continue;
						}
						IInteractableGameActor target = potentialTargets[0];
						foreach (IInteractableGameActor actor in actors[effect.target.targetType]) {
							int actorHealth = healthTracker[actor];
							if (actorHealth <= 0) {
								continue;
							}
							int remaining = actorHealth - damageSum;

							if ((closest > 0 && remaining < closest) ||
								(closest < 0 && remaining > closest && remaining <= 0)
							) {
								closest = remaining;
								target = actor;
							}
						}

						effectsToTargets[effect].Add(target);
						healthTracker[target] -= damageSum;
					}
				}

				unresolvedEffects.RemoveAll(x => x.target.Quantity() == TargetQuantity.ONE);

				foreach (Effect effect in unresolvedEffects) {
					Debug.LogError("Simple enemy doesn't support ManyTarget effects yet");
				}

				return effectsToTargets;
			}

		}

	}
}
