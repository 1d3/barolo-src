﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;
using System.Collections.Generic;

namespace Barolo {

	using Abilities;
	using AI;
	using Events;
	using Encounter.MajorEncounter;
	using Encounter.Resource;

	namespace Encounter.Enemies {

		public abstract class Enemy : MonoBehaviour, IAiActor, IInteractableGameActor {

			public EnemyData data;
			public GameObject ActorGameObject {
				get { return this.gameObject; }
				set { }
			}
			public ZoneType CurrentZone { get; set; }


			protected StateController controller;

			void Awake() {
				controller = GetComponent<StateController>();
			}

			void Start() {
				InitResources();
			}

			public void InitResources() {
				GetResource<Health>().Init(data.maxHealth, data.maxHealth);
			}

			public void DoActions() {
				controller.TransitionAndAct();
			}

			public void Execute(CommandType command) {
				Dictionary<Effect, HashSet<IInteractableGameActor>> abilitiesToUse = DetermineActions(command);
				ExecuteAbilities(abilitiesToUse);
			}

			public void ClickOn() {
				
			}

			public void ChangeZone(ZoneType destination) {
				CurrentZone = destination;
			}

			public void DealDamage(IInteractableGameActor source, DamageType type, int amount) {
				ApplyDamage(source, type, amount);
				if (this.IsDefeated()) {
					this.TriggerDeath();
				}
			}

			protected abstract Dictionary<Effect, HashSet<IInteractableGameActor>> DetermineActions(CommandType command);
			
			protected void ExecuteAbilities(Dictionary<Effect, HashSet<IInteractableGameActor>> effectsToTargets) {
				foreach (Effect effect in effectsToTargets.Keys) {
					effect.Resolve(this, effectsToTargets[effect]);
				}
			}

			protected virtual void ApplyDamage(IInteractableGameActor source, DamageType type, int amount) {
				ChangeResource<Health>(source, amount);
				DamageDealtEventArgs damageDealtArgs = new DamageDealtEventArgs(amount, type, source, this, GetResource<Health>().current);
				EventManager.AddToQueue(EventName.DAMAGE_DEALT, damageDealtArgs);
			}

			public virtual bool IsDefeated() {
				return GetResource<Health>().current <= 0;
			}

			public void TriggerDeath() {
				HandleDeath();
				EnemyDeathEventArgs args = new EnemyDeathEventArgs(this);
				EventManager.AddToQueue(EventName.ENEMY_DEATH, args);
			}

			protected virtual void HandleDeath() {
				Destroy(this.gameObject);
			}

			public T GetResource<T>() where T : BaseResource {
				return this.ActorGameObject.GetComponent<T>();
			}

			public void ChangeResource<T>(IInteractableGameActor source, int amount) where T : BaseResource {
				BaseResource resource = GetResource<T>();
				if (resource != null) {
					resource.Pay(amount);
				}
			}

		}
	}
}
