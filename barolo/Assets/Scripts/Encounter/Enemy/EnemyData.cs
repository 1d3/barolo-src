﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.UI;
using System;

namespace Barolo {

	using Abilities;

	namespace Encounter.Enemies {

		[Serializable]
		[CreateAssetMenu(menuName="Enemy/Enemy Data")]
		public class EnemyData : ScriptableObject {

			public int maxHealth;
			public EnemyType enemyType;
			public EnemySubType[] enemySubTypes;
			public ActivatedAbility[] abilities;
			
		}

	}
}
