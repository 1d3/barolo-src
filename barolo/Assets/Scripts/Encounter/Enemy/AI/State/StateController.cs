﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace AI {

		public class StateController : MonoBehaviour {

			public State currentState;
			public State remainState;
			public IAiActor actor;

			void Awake() {
				actor = GetComponent<IAiActor>();
			}

			public void SendCommand(CommandType command) {
				actor.Execute(command);
			}

			public void TransitionAndAct() {
				currentState.CheckTransitions(this);
				currentState.DoActions(this);
			}


			public void TransitionToState(State nextState) {
				if (nextState != remainState) {
					currentState = nextState;
				}
			}

		}

	}
}