﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace AI {

		[CreateAssetMenu (menuName = "AI/State")]
		public class State : ScriptableObject {

			public Action[] actions;
			public Transition[] transitions;

			public void EnterState(StateController controller) {
				DoActions(controller);
			}

			public void DoActions(StateController controller) {
				foreach (Action action in actions) {
					action.Act(controller);
				}
			}

			public void CheckTransitions (StateController controller) {
				foreach (Transition transition in transitions) {
					bool decision = transition.decision.Decide(controller);
					State nextState = transition.GetState(decision);
					controller.TransitionToState(nextState);
				}

			}
		}

	}
}