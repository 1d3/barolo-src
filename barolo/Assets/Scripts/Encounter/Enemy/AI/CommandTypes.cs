﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace AI {

		public enum CommandType {
			NOTHING,
			LARGE_DAMAGE,
			DAMAGE_ARTIFACTS,
			DEFEND,
			HEAL
		}

	}
}