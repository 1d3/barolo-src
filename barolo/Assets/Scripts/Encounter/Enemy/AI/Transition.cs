﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;

namespace Barolo {
	namespace AI {

		[Serializable]
		public class Transition {
			public Decision decision;
			public State trueState;
			public State falseState;

			public State GetState(bool which) {
				return which ? trueState : falseState;
			}
		}

	}
}