﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace AI {

		public abstract class Decision : ScriptableObject {

			public abstract bool Decide(StateController controller);
		}

	}
}