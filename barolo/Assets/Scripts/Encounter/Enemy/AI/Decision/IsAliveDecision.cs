﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Encounter.Resource;

	namespace AI {

		[CreateAssetMenu(menuName="AI/Decisions/Is Alive")]
		public class IsAliveDecision : Decision {

			public override bool Decide(StateController controller) {

				Health health = controller.actor.GetResource<Health>();
				if (health == null) {
					return false;
				}

				return health.current > 0;

			}

		}

	}
}
