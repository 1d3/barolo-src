﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {

	using Encounter.Resource;

	namespace AI {

		public interface IAiActor {

			void Execute(CommandType command);

			T GetResource<T>() where T : BaseResource;

		}

	}
}