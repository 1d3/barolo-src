﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace AI {

		public abstract class Action : ScriptableObject {

			public abstract void Act(StateController controller);
		}

	}
}