﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace AI {

		[CreateAssetMenu(menuName="AI/Actions/Large Damage Action")]
		public class LargeDamageAction : Action {

			public override void Act(StateController controller) {
				controller.SendCommand(CommandType.LARGE_DAMAGE);
			}

		}

	}
}