﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using UnityEngine.UI;

namespace Barolo {

	using Events;

	namespace Encounter.UI {

		public class InteractableButton : MonoBehaviour {

			protected Button button;
			public ButtonType buttonType;

			void Awake() {
				button = GetComponent<Button>();
			}

			void Start() {
				button.onClick.AddListener(ButtonClick);
				this.gameObject.SetActive(false);
			}

			void ButtonClick() {
				ButtonEventArgs args = new ButtonEventArgs(this.buttonType);
				EventManager.AddToQueue(EventName.BUTTON_CLICK, args);
			}

		}

	}
}
