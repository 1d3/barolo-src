﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Encounter.UI {

		public enum ButtonType {
			DONE,
			CONFIRM,
			CANCEL
		}

	}
}
