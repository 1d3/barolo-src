﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Events;
	using Cards;
	using Cards.UI;

	namespace Encounter.UI {

		public class Hand : MonoBehaviour {

			private Player player;

			void Awake() {
				this.player = GameObject.FindObjectOfType<Player>();
			}

			void OnEnable() {
				EventManager.StartListening(EventName.DREW_CARD, OnDrewCard);
			}

			void OnDisable() {
				EventManager.StopListening(EventName.DREW_CARD, OnDrewCard);
			}

			void OnDrewCard(GameEventArgs args) {
				PlayerDrewCardEventArgs drewArgs = (PlayerDrewCardEventArgs)args;
				if (drewArgs.drawnCard != null) {
					AddCardToHand(drewArgs.drawnCard);
				}
			}

			public void AddCardToHand(Card card) {
				card.AwakeFromDeck(this.gameObject.transform);

				TemplateUI cardUi = card.ActorGameObject.GetComponent<TemplateUI>();
				CardUpgradeLevel activeUpgradeLevel = card.GetLevelFromStats(player.GetAffinity(card.data.faction));

				cardUi.Refresh(activeUpgradeLevel);
			}

		}

	}
}
