﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {

	using Events;
	using Cards;
	using Encounter.Resource;

	public class Player : MonoBehaviour, IInteractableGameActor {

		public GameObject ActorGameObject {
			get { return this.gameObject; }
			set { }
		}
		public ZoneType CurrentZone {
			get { return ZoneType.PLAYER; }
			set { }
		}

		[HideInInspector] public List<Card> hand = new List<Card>();

		private Deck _deck = new Deck(new Card[0]{});

		void Start() {
			InitResources();
		}

		void OnEnable() {
			EventManager.StartListening(EventName.REFRESH_RESOURCES, OnRefreshResources);
		}

		void OnDisable() {
			EventManager.StopListening(EventName.REFRESH_RESOURCES, OnRefreshResources);
		}

		public void InitResources() {
			this.ActorGameObject.GetComponent<Health>().Init(5, 5);
			this.ActorGameObject.GetComponent<ActionPoints>().Init(3, 3);
		}

		public void SetUpDeck(Deck deck) {
			_deck = deck;
			DeckSetupEventArgs deckArgs = new DeckSetupEventArgs(deck);
			EventManager.AddToQueue(EventName.DECK_SETUP, deckArgs);
		}

		public Card DrawCard() {
			Card card = _deck.DrawCard();
			if (card != null) {
				card.ActivateLevelFromStats(GetAffinity(card.data.faction));
			}
			return card;
		}

		public void ClickOn() {
			
		}

		public void DealDamage(IInteractableGameActor source, DamageType type, int amount) {
			this.ChangeResource<Health>(source, amount);
			Health health = GetResource<Health>();
			DamageDealtEventArgs damageArgs = new DamageDealtEventArgs(amount, type, source, this, health.current);
			EventManager.AddToQueue(EventName.DAMAGE_DEALT, damageArgs);
			if (health.current == 0) {
				EndEncounterEventArgs args = new EndEncounterEventArgs(true);
				EventManager.AddToQueue(EventName.END_ENCOUNTER, args);
			}
		}

		public void ChangeZone(ZoneType destination) {
			Debug.LogError("Trying to change the zone type of player");
		}

		public int GetAttribute(AttributeType attribute) {
			return 5;
		}

		public int GetAffinity(FactionType faction) {
			return 2;
		}

		public void AddCardToHand(Card card) {
			hand.Add(card);
		}

		public void ChangeResource<T>(IInteractableGameActor source, int amount) where T : BaseResource {
			BaseResource resource = this.ActorGameObject.GetComponent<T>();
			if (resource != null) {
				resource.Pay(amount);
			}
		}

		public T GetResource<T>() where T : BaseResource {
			return this.ActorGameObject.GetComponent<T>();
		}

		void OnRefreshResources(GameEventArgs args) {
			this.ActorGameObject.GetComponent<ActionPoints>().Pay(-2);
		}

		public void DrawCards(int amount) {
			for (int i = 0; i < amount; i++) {
				Card card = DrawCard();
				AddCardToHand(card);
				PlayerDrewCardEventArgs drewArgs = new PlayerDrewCardEventArgs(card, _deck.libraryCount);
				EventManager.AddToQueue(EventName.DREW_CARD, drewArgs);
			}
		}

		void OnSetupDeck(GameEventArgs args) {
			DeckSetupEventArgs deckArgs = (DeckSetupEventArgs)args;
			SetUpDeck(deckArgs.deck);
		}
	}

}
