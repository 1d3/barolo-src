﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Barolo {

	using Abilities;
	using Cards;
	using Cards.UI;
	using Events;
	using Encounter.UI;

	namespace Encounter.MajorEncounter.UI {

		public class MajorEncounterUI : MonoBehaviour {

			public Transform battlefield;
			public Transform deck;
			public Transform discard;

			public Button doneButton;
			public Button confirmButton;
			public Button cancelButton;

			protected bool choosingTargets = false;
			protected int numTargetsChosen = 0;

			void OnEnable() {
				EventManager.StartListening(EventName.CHANGE_ZONE, OnChangeZone);
				EventManager.StartListening(EventName.CHOOSING_TARGET, OnChoosingTargets);
				EventManager.StartListening(EventName.TARGET_CHOSEN, OnTargetChosen);
				EventManager.StartListening(EventName.ALL_TARGETS_CHOSEN, OnAllTargetsChosen);
				EventManager.StartListening(EventName.BUTTON_CLICK, OnButtonClick);
				EventManager.StartListening(EventName.PAYING_COST, OnPayingCost);
				EventManager.StartListening(EventName.COST_PAID, OnCostPaid);
				EventManager.StartListening(EventName.ALL_COSTS_PAID, OnAllCostsPaid);
			}

			void OnDisable() {
				EventManager.StopListening(EventName.CHANGE_ZONE, OnChangeZone);
				EventManager.StopListening(EventName.CHOOSING_TARGET, OnChoosingTargets);
				EventManager.StopListening(EventName.TARGET_CHOSEN, OnTargetChosen);
				EventManager.StopListening(EventName.ALL_TARGETS_CHOSEN, OnAllTargetsChosen);
				EventManager.StopListening(EventName.BUTTON_CLICK, OnButtonClick);
				EventManager.StopListening(EventName.PAYING_COST, OnPayingCost);
				EventManager.StopListening(EventName.COST_PAID, OnCostPaid);
				EventManager.StopListening(EventName.ALL_COSTS_PAID, OnAllCostsPaid);
			}

			void Start() {
				doneButton.gameObject.SetActive(true);
			}

			void TransitionToDefaultState() {
				choosingTargets = false;
				numTargetsChosen = 0;
				doneButton.gameObject.SetActive(true);
				confirmButton.gameObject.SetActive(false);
				cancelButton.gameObject.SetActive(false);
			}

			void OnChangeZone(GameEventArgs args) {
				ChangeZoneEventArgs changeArgs = (ChangeZoneEventArgs)args;
				IInteractableGameActor source = changeArgs.source;
				Transform destination = source.ActorGameObject.transform;
				switch (changeArgs.end) {
					case ZoneType.BATTLEFIELD:
						destination = battlefield;
						break;
					case ZoneType.DECK:
						destination = deck;
						break;
					case ZoneType.DISCARD:
						destination = discard;
						break;
					default:
						break;
				}
				source.ChangeZone(changeArgs.end);
				source.ActorGameObject.transform.SetParent(destination);
			}

			void OnChoosingTargets(GameEventArgs args) {
				cancelButton.gameObject.SetActive(true);
				doneButton.gameObject.SetActive(false);
				ChoosingTargetEventArgs targetArgs = (ChoosingTargetEventArgs)args;
				if (targetArgs.effect.target.Quantity() == TargetQuantity.MANY) {
					choosingTargets = true;
				}
			}

			void OnTargetChosen(GameEventArgs args) {
				if (choosingTargets) {
					TargetChosenEventArgs targetArgs = (TargetChosenEventArgs)args;
					numTargetsChosen += 1;
					if (numTargetsChosen >= targetArgs.effect.target.Min()) {
						confirmButton.gameObject.SetActive(true);
					}
				}
			}

			void OnAllTargetsChosen(GameEventArgs args) {
				TransitionToDefaultState();
			}

			void OnButtonClick(GameEventArgs args) {
				ButtonEventArgs buttonArgs = (ButtonEventArgs)args;
				if (buttonArgs.buttonType == ButtonType.CANCEL) {
					TransitionToDefaultState();
				}
			}

			void OnPayingCost(GameEventArgs args) {
				cancelButton.gameObject.SetActive(true);
				doneButton.gameObject.SetActive(false);
			}

			void OnCostPaid(GameEventArgs args) {
				cancelButton.gameObject.SetActive(false);
			}

			void OnAllCostsPaid(GameEventArgs args) {}

		}

	}
}
