﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using UnityEngine.UI;

namespace Barolo {

	using Events;

	namespace Encounter.MajorEncounter.UI {

		public class DeckUI : MonoBehaviour {

			public Text libraryCount;

			void OnEnable() {
				EventManager.StartListening(EventName.DREW_CARD, OnDrewCard);
			}

			void OnDisable() {
				EventManager.StopListening(EventName.DREW_CARD, OnDrewCard);
			}

			void OnDrewCard(GameEventArgs args) {
				PlayerDrewCardEventArgs drewArgs = (PlayerDrewCardEventArgs)args;
				libraryCount.text = drewArgs.libraryCount.ToString();
			}

		}

	}
}
