﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using System;
using System.Collections.Generic;

namespace Barolo {

	using Events;
	using Abilities;

	namespace Encounter.MajorEncounter {

		public class ChooseTargetsState : MajorEncounterState {

			private IInteractableGameActor source;

			private Dictionary<Effect, HashSet<IInteractableGameActor>> effectsToTargets;
			private Stack<Effect> effectsNeedTargets;
			private Effect currentEffect;

			public override void OnEnterState(GameEventArgs args) {
				AllCostsPaidEventArgs costArgs = (AllCostsPaidEventArgs)args;
				source = costArgs.source;
				effectsToTargets = new Dictionary<Effect, HashSet<IInteractableGameActor>>();
				effectsNeedTargets = new Stack<Effect>(costArgs.ability.effects);

				TransitionToNextEffect();
			}

			public override void HandleClick(ClickEventArgs args) {
				IInteractableGameActor clickTarget = args.target;

				if (clickTarget.ActorGameObject.CompareTag(currentEffect.target.TargetTag()) &&
				 	Array.Exists(currentEffect.targetZones, element => element == clickTarget.CurrentZone)) {

					if (effectsToTargets[currentEffect].Add(clickTarget)) {
						TargetChosenEventArgs targetArgs = new TargetChosenEventArgs(source, currentEffect, clickTarget);
						EventManager.AddToQueue(EventName.TARGET_CHOSEN, targetArgs);

						if (currentEffect.target.Quantity() == TargetQuantity.ONE) {
							TransitionToNextEffect();
						}
					}

				}
			}

			public override void Confirm(GameEventArgs args) {
				if (effectsToTargets[currentEffect].Count >= currentEffect.target.Min()) {
					TransitionToNextEffect();
				}
			}

			public override void Cancel(GameEventArgs args) {
				MajorEncounterManager.TransitionToState(StateName.PAY_COSTS, args);
			}

			void TransitionToNextEffect() {
				if (effectsNeedTargets.Count > 0) {
					currentEffect = effectsNeedTargets.Pop();
					effectsToTargets[currentEffect] = currentEffect.GetDefaultTargets();

					if (currentEffect.target.Quantity() == TargetQuantity.ALL ||
						currentEffect.target.Quantity() == TargetQuantity.NONE) {
							TransitionToNextEffect();
						} else {
							ChoosingTargetEventArgs args = new ChoosingTargetEventArgs(currentEffect);
							EventManager.AddToQueue(EventName.CHOOSING_TARGET, args);
						}

				} else {
					AllTargetsChosenEventArgs args = new AllTargetsChosenEventArgs(this.source, effectsToTargets);
					EventManager.AddToQueue(EventName.ALL_TARGETS_CHOSEN, args);
				}
			}

		}

	}
}
