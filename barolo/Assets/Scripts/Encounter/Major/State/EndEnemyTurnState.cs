﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Events;

	namespace Encounter.MajorEncounter {

		public class EndEnemyTurnState : MajorEncounterState {

			public override void OnEnterState(GameEventArgs args) {
				MajorEncounterManager.AdvanceTurn(args);
			}

		}

	}
}
