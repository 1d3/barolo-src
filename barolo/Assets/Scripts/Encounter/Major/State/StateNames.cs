﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	namespace Encounter.MajorEncounter {

		public enum StateName {
			START,
			BEGIN_GAME,
			PLAYER_TURN,
			ENEMY_TURN,
			PAY_COSTS,
			CHOOSE_TARGETS,
			END_PLAYER_TURN,
			END_ENEMY_TURN
		}

	}
}
