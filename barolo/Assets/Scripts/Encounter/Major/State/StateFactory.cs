﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {

	namespace Encounter.MajorEncounter {

		public class StateFactory {

			protected Dictionary<StateName, MajorEncounterState> states;

			public StateFactory() {
				states = new Dictionary<StateName, MajorEncounterState> {
					{ StateName.START, new StartState() },
					{ StateName.BEGIN_GAME, new BeginGameState() },
					{ StateName.PLAYER_TURN, new PlayerTurnState() },
					{ StateName.ENEMY_TURN, new EnemyTurnState() },
					{ StateName.PAY_COSTS, new PayCostsState() },
					{ StateName.CHOOSE_TARGETS, new ChooseTargetsState() },
					{ StateName.END_PLAYER_TURN, new EndPlayerTurnState() },
					{ StateName.END_ENEMY_TURN, new EndEnemyTurnState() }
				};
			}

			public MajorEncounterState GetState(StateName name) {
				return states[name];
			}

		}

	}
}
