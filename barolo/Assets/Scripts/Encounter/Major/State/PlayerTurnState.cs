﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Events;

	namespace Encounter.MajorEncounter {

		public class PlayerTurnState : MajorEncounterState {

			private bool lastRefreshTurn = true;

			public override void OnEnterState(GameEventArgs args) {
				if (lastRefreshTurn != MajorEncounterManager.Instance.turnBit) {
					RefreshResourcesEventArgs refreshArgs = new RefreshResourcesEventArgs();
					EventManager.AddToQueue(EventName.REFRESH_RESOURCES, refreshArgs);
					
					Player player = GameObject.FindGameObjectWithTag(Constants.Tags.PLAYER_TAG).GetComponent<Player>();
					player.DrawCards(Constants.Game.MAJOR_ENCOUNTER_TURN_DRAW_NUM);

					lastRefreshTurn = !lastRefreshTurn;
				}
			}

			public override void HandleClick(ClickEventArgs args) {
				args.target.ClickOn();
			}

		}

	}
}
