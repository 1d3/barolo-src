﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	
	using Events;
	using Encounter.Enemies;

	namespace Encounter.MajorEncounter {

		public class EnemyTurnState : MajorEncounterState {

			public override void OnEnterState(GameEventArgs args) {
				GameObject[] enemies = GameObject.FindGameObjectsWithTag(Constants.Tags.ENEMY_TAG);
				foreach (GameObject enemyGo in enemies) {
					Enemy enemy = enemyGo.GetComponent<Enemy>();
					enemy.DoActions();
				}
				MajorEncounterManager.TransitionToState(StateName.END_ENEMY_TURN, args);
			}

		}

	}
}
