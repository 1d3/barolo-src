﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Events;

	namespace Encounter.MajorEncounter {

		public class EndPlayerTurnState : MajorEncounterState {

			public override void OnEnterState(GameEventArgs args) {
				MajorEncounterManager.TransitionToState(StateName.ENEMY_TURN, args);
			}

		}

	}
}
