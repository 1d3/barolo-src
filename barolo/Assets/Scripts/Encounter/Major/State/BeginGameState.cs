﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using SaveLoad;
	using SaveLoad.Bundles;
	using Cards;
	using Events;
	using Progress;

	namespace Encounter.MajorEncounter {

		public class BeginGameState : MajorEncounterState {

			public override void HandleClick(ClickEventArgs args) {
				
			}

			public override void OnEnterState(GameEventArgs args) {
				GameObject progressGo = GameObject.FindGameObjectWithTag(Constants.Tags.PROGRESS_TAG);
				DeckProgress deckProgress = progressGo.GetComponent<DeckProgress>();
				Player player = GameObject.FindGameObjectWithTag(Constants.Tags.PLAYER_TAG).GetComponent<Player>();
				Deck playerDeck = deckProgress.LoadDeck();
				playerDeck.Shuffle();
				
				EventManager.AddToQueue(EventName.SPAWN_ENEMY, null);
				player.SetUpDeck(playerDeck);
				player.DrawCards(Constants.Game.MAJOR_ENCOUNTER_START_DRAW_NUM);
				MajorEncounterManager.TransitionToState(StateName.PLAYER_TURN, null);
			}

		}

	}
}
