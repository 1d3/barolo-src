﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {

	using Events;
	using Abilities;

	namespace Encounter.MajorEncounter {

		public class PayCostsState : MajorEncounterState {

			private Cost currentCost;
			private Stack<Cost> costsToPay;
			private IInteractableGameActor source;
			private ActivatedAbility abilityBeingActivated;
			private Dictionary<Cost, HashSet<IInteractableGameActor>> paidCosts;

			public override void OnEnterState(GameEventArgs args) {
				if (args.GetType() == typeof(ButtonEventArgs)) {
					this.Cancel(args);
					return;
				}

				ActivateAbilityEventArgs abilityArgs = (ActivateAbilityEventArgs)args;
				costsToPay = new Stack<Cost>(abilityArgs.ability.costs);
				paidCosts = new Dictionary<Cost, HashSet<IInteractableGameActor>>();
				source = abilityArgs.source;
				abilityBeingActivated = abilityArgs.ability;

				TransitionToNextCost();
			}

			public override void HandleClick(ClickEventArgs args) {
				IInteractableGameActor target = args.target;

				if (currentCost.PayWith(target)) {
					paidCosts[currentCost].Add(target);
					if (currentCost.ActorsCompletePay(source, paidCosts[currentCost])) {
						InputCostPaidEventArgs costArgs = new InputCostPaidEventArgs(currentCost);
						EventManager.AddToQueue(EventName.COST_PAID, costArgs);
						TransitionToNextCost();
					}
				}
			}

			public override void Cancel(GameEventArgs args) {
				foreach (Cost cost in paidCosts.Keys) {
					cost.UndoPay(source);
					HashSet<IInteractableGameActor> actorsPaid = paidCosts[cost];
					foreach (IInteractableGameActor actor in actorsPaid) {
						cost.UndoPayWith(actor);
					}
				}
				MajorEncounterManager.TransitionToState(StateName.PLAYER_TURN, args);
			}

			public void TransitionToNextCost() {
				if (costsToPay.Count > 0) {
					currentCost = costsToPay.Pop();
					
					if (currentCost.canAutomate) {
						if (currentCost.AutomatedPay(source)) {
							paidCosts.Add(currentCost, new HashSet<IInteractableGameActor>());
							TransitionToNextCost();
						} else {
							Cancel(null);
						}
					} else {
						paidCosts.Add(currentCost, new HashSet<IInteractableGameActor>());
						PayingInputCostEventArgs args = new PayingInputCostEventArgs(currentCost);
						EventManager.AddToQueue(EventName.PAYING_COST, args);
					}
				} else {
					AllCostsPaidEventArgs args = new AllCostsPaidEventArgs(source, abilityBeingActivated);
					EventManager.AddToQueue(EventName.ALL_COSTS_PAID, args);
				}
			}

		}

	}
}
