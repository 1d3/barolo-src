﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Events;

	namespace Encounter.MajorEncounter {

		public abstract class MajorEncounterState {

			public virtual void HandleClick(ClickEventArgs args) {}

			public virtual void OnEnterState(GameEventArgs args) {}

			public virtual void OnExitState(GameEventArgs args) {}

			public virtual void Confirm(GameEventArgs args) {}

			public virtual void Cancel(GameEventArgs args) {}

		}

	}
}
