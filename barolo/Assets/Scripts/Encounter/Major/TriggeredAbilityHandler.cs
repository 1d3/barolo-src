﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Barolo {

	using Abilities;

	namespace Encounter.MajorEncounter {

		public static class TriggeredAbilityHandler {

			public static List<AbilityData> triggers = new List<AbilityData>();

			public static void ToggleTrigger(IInteractableGameActor source, TriggeredAbility ability, bool active) {
				if (active) {
					RegisterTrigger(source, ability);
				} else {
					RemoveTrigger(source, ability);
				}
			}

			public static void RegisterTrigger(IInteractableGameActor source, TriggeredAbility ability) {
				AbilityData data = new AbilityData(source, ability);
				triggers.Add(data);
			}

			public static void RemoveTrigger(IInteractableGameActor source, TriggeredAbility ability) {
				AbilityData itemToRemove = default(AbilityData);
				foreach (AbilityData data in triggers) {
					if (data.source == source && data.ability == ability) {
						itemToRemove = data;
						break;
					} 
				}
				triggers.Remove(itemToRemove);
			}

			public static void ResolveTriggers() {
				foreach (AbilityData data in triggers) {
					if (data.ability.CanAct(data.source)) {
						foreach (Effect effect in data.ability.effects) {
							HashSet<IInteractableGameActor> targets = effect.GetDefaultTargets();
							effect.Resolve(data.source, targets);
						}
					}
					data.ability.condition.ResetState();
				}
			}

			public struct AbilityData {
				public IInteractableGameActor source;
				public TriggeredAbility ability;

				public AbilityData(IInteractableGameActor source, TriggeredAbility ability) {
					this.source = source;
					this.ability = ability;
				}
			}

		}

	}
}
