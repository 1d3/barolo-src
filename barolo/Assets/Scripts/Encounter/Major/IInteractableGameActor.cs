﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {

	using Encounter.Resource;

	public interface IInteractableGameActor {

		void ClickOn();

		ZoneType CurrentZone { get; set; }

		void ChangeZone(ZoneType destination);

		GameObject ActorGameObject { get; set; }

		void DealDamage(IInteractableGameActor source, DamageType damageType, int amount);

		void ChangeResource<T>(IInteractableGameActor source, int amount) where T : BaseResource;

		T GetResource<T>() where T : BaseResource;

	}

}
