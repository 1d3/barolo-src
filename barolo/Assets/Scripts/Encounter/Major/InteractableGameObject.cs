﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Barolo {

	using Events;

	public class InteractableGameObject : MonoBehaviour, IPointerClickHandler {

		public IInteractableGameActor owner;

		void Awake() {
			owner = this.GetComponent<IInteractableGameActor>();
		}

		public void OnPointerClick(PointerEventData pointerEventData) {
			ClickEventArgs args = new ClickEventArgs(owner);
			EventManager.AddToQueue(EventName.CLICK, args);
		}

	}

}
