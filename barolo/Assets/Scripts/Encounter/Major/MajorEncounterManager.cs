﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Barolo {

	using Events;
	using Cards;
	using Abilities;
	using Encounter.UI;
	using SaveLoad;
	using Progress;

	namespace Encounter.MajorEncounter {

		public class MajorEncounterManager : MonoBehaviour {

			public static MajorEncounterManager Instance { get; private set; }

			private StateFactory stateFactory;
			public StateName currentState;
			
			[HideInInspector] public bool turnBit;

			void Awake() {
				if (Instance != null && Instance != this) {
					Destroy(this);
				} else {
					Instance = this;
					Instance.Init();
				}
			}

			void Init() {
				this.stateFactory = new StateFactory();
				this.currentState = StateName.START;
				this.turnBit = true;
			}

			void OnEnable() {
				EventManager.StartListening(EventName.DREW_CARD, OnDrewCards);
				EventManager.StartListening(EventName.DECK_SETUP, OnDeckSetup);
				EventManager.StartListening(EventName.CLICK, OnClick);
				EventManager.StartListening(EventName.ACTIVATE_ABILITY, OnActivateAbility);
				EventManager.StartListening(EventName.ALL_TARGETS_CHOSEN, OnAllTargetsChosen);
				EventManager.StartListening(EventName.BUTTON_CLICK, OnButtonClick);
				EventManager.StartListening(EventName.ALL_COSTS_PAID, OnAllCostsPaid);
				EventManager.StartListening(EventName.ENEMY_DEATH, OnEnemyDeath);
				EventManager.StartListening(EventName.END_ENCOUNTER, OnEncounterEnd);
			}

			void OnDisable() {
				EventManager.StopListening(EventName.DREW_CARD, OnDrewCards);
				EventManager.StopListening(EventName.DECK_SETUP, OnDeckSetup);
				EventManager.StopListening(EventName.CLICK, OnClick);
				EventManager.StopListening(EventName.ACTIVATE_ABILITY, OnActivateAbility);
				EventManager.StopListening(EventName.ALL_TARGETS_CHOSEN, OnAllTargetsChosen);
				EventManager.StopListening(EventName.BUTTON_CLICK, OnButtonClick);
				EventManager.StopListening(EventName.ALL_COSTS_PAID, OnAllCostsPaid);
				EventManager.StopListening(EventName.ENEMY_DEATH, OnEnemyDeath);
				EventManager.StopListening(EventName.END_ENCOUNTER, OnEncounterEnd);
			}

			void Start() {
				SceneManager.SetActiveScene(this.gameObject.scene);
				TransitionToState(StateName.BEGIN_GAME, null);
			}

			public static void AdvanceTurn(GameEventArgs args) {
				Instance.turnBit = !Instance.turnBit;
				TransitionToState(StateName.PLAYER_TURN, args);
			}

			public static void TransitionToState(StateName newState, GameEventArgs args) {
				GetState().OnExitState(args);
				Instance.currentState = newState;
				GetState().OnEnterState(args);
			}

			static MajorEncounterState GetState() {
				return Instance.stateFactory.GetState(Instance.currentState);
			}

			void OnClick(GameEventArgs args) {
				ClickEventArgs clickArgs = (ClickEventArgs)args;
				GetState().HandleClick(clickArgs);
			}

			void OnButtonClick(GameEventArgs args) {
				ButtonEventArgs buttonArgs = (ButtonEventArgs)args;
				switch (buttonArgs.buttonType) {
					case ButtonType.DONE:
						TransitionToState(StateName.END_PLAYER_TURN, args);
						break;
					case ButtonType.CONFIRM:
						GetState().Confirm(args);
						break;
					case ButtonType.CANCEL:
						GetState().Cancel(args);
						break;
					default:
						break;
				}
			}

			void OnEncounterEnd(GameEventArgs args) {
				GameObject progressGo = GameObject.FindGameObjectWithTag(Constants.Tags.PROGRESS_TAG);
				DeckProgress deckProgress = progressGo.GetComponent<DeckProgress>();
				deckProgress.SaveDeck(DeckSaveLoad.FromGame());
			}

			void OnDrewCards(GameEventArgs args) {
				PlayerDrewCardEventArgs drewArgs = (PlayerDrewCardEventArgs)args;
				if (drewArgs.drawnCard != null) {
					ChangeZoneEventArgs changeArgs = new ChangeZoneEventArgs(drewArgs.drawnCard, ZoneType.DECK, ZoneType.HAND);
					EventManager.AddToQueue(EventName.CHANGE_ZONE, changeArgs);
				}
			}

			void OnDeckSetup(GameEventArgs args) {
				DeckSetupEventArgs deckArgs = (DeckSetupEventArgs)args;
				Card[] cardsInDeck = deckArgs.deck.GetCards();

				foreach (Card card in cardsInDeck) {
					ChangeZoneEventArgs changeArgs = new ChangeZoneEventArgs(card, ZoneType.NONE, ZoneType.DECK);
					EventManager.AddToQueue(EventName.CHANGE_ZONE, changeArgs);
				}
			}

			void OnActivateAbility(GameEventArgs args) {
				TransitionToState(StateName.PAY_COSTS, args);
			}

			void OnAllTargetsChosen(GameEventArgs args) {
				AllTargetsChosenEventArgs targetsArgs = (AllTargetsChosenEventArgs)args;
				foreach (Effect effect in targetsArgs.effectsToTargets.Keys) {
					effect.Resolve(targetsArgs.source, targetsArgs.effectsToTargets[effect]);
				}
				TriggeredAbilityHandler.ResolveTriggers();
				TransitionToState(StateName.PLAYER_TURN, null);
			}

			void OnAllCostsPaid(GameEventArgs args) {
				TransitionToState(StateName.CHOOSE_TARGETS, args);
			}

			void OnEnemyDeath(GameEventArgs args) {
				GameObject[] enemies = GameObject.FindGameObjectsWithTag(Constants.Tags.ENEMY_TAG);
				if (enemies.Length == 0) {
					EndEncounterEventArgs endEncounterArgs = new EndEncounterEventArgs(false);
					EventManager.AddToQueue(EventName.END_ENCOUNTER, endEncounterArgs);
				}
			}

		}

	}
}
