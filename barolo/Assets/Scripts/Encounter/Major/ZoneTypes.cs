﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {

	public enum ZoneType {
		NONE,
		HAND,
		BATTLEFIELD,
		DISCARD,
		DECK,
		PLAYER

	}

}
