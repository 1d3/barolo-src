﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Barolo {

    using SaveLoad.Bundles;
    using Events;

    namespace Encounter.MinorEncounter {

        public class MinorEncounterManager : MonoBehaviour {

            public Player player;

            private MinorEncounterNode node;

            private static MinorEncounterManager _instance;

            public static MinorEncounterManager Instance {
                get { return _instance; }
            }

            void Awake() {
                if (_instance == null) {
                    _instance = this.GetComponent<MinorEncounterManager>();
                }
            }

            void OnEnable() {
                EventManager.StartListening(EventName.CHOICE_SELECTED, OnChoiceSelected);
                EventManager.StartListening(EventName.SKILL_CHECK_ENCOUNTERED, OnSkillCheckEncountered);
                EventManager.StartListening(EventName.SKILL_CHECK_COMPLETE, OnSkillCheckComplete);
            }

            void OnDisable() {
                EventManager.StopListening(EventName.CHOICE_SELECTED, OnChoiceSelected);
                EventManager.StopListening(EventName.SKILL_CHECK_ENCOUNTERED, OnSkillCheckEncountered);
                EventManager.StopListening(EventName.SKILL_CHECK_COMPLETE, OnSkillCheckComplete);
            }

            void Start() {
                SceneManager.SetActiveScene(this.gameObject.scene);
                this.node = SetupEncounter("PopplingPanic");
                ExecuteNode(this.node);
            }

            void ExecuteNode(MinorEncounterNode nodeToExecute) {
                NodeAdvancedEventArgs args = new NodeAdvancedEventArgs();
                EventManager.AddToQueue(EventName.NODE_ADVANCED, args);
                nodeToExecute.Execute();
            }

            MinorEncounterNode SetupEncounter(string encounterName) {
                MinorEncounter encounter = MinorEncounterLoader.LoadMinorEncounter(encounterName, Constants.Bundles.MinorEncountersBundle);
                return encounter;
            }

            void AdvanceToNode(MinorEncounterNode nextNode) {
                this.node = nextNode;
                ExecuteNode(nextNode);
            }

            void OnChoiceSelected(GameEventArgs args) {
                ChoiceSelectedEventArgs choiceArgs = (ChoiceSelectedEventArgs)args;
                MinorEncounterNode nextNode = ResolveChoiceSelected(this.node, choiceArgs);
                AdvanceToNode(nextNode);
            }

            void OnSkillCheckEncountered(GameEventArgs args) {
                AdvanceToNode(node.checkNode);
            }

            void OnSkillCheckComplete(GameEventArgs args) {
                SkillCheckCompleteEventArgs skillArgs = (SkillCheckCompleteEventArgs)args;
                MinorEncounterNode nextNode = ResolveSkillCheck((CheckNode)this.node, skillArgs);
                AdvanceToNode(nextNode);
            }

            public static MinorEncounterNode ResolveChoiceSelected(MinorEncounterNode currentNode, ChoiceSelectedEventArgs args) {
                MinorEncounterNode nextNode = null;
                foreach (ChoiceNode choiceNode in currentNode.choices) {
                    if (choiceNode.key == args.choiceKey) {
                        nextNode = choiceNode;
                        break;
                    }
                }
                if (nextNode == null) {
                    Debug.LogError("Could not find ChoiceNode that matches choice made");
                }
                return nextNode;
            }

            public static MinorEncounterNode ResolveSkillCheck(CheckNode currentNode, SkillCheckCompleteEventArgs args) {
                MinorEncounterNode nextNode = null;
                foreach (ResultNode result in currentNode.results) {
                    if (args.checkValue <= result.max && args.checkValue >= result.min) {
                        nextNode = result;
                        break;
                    }
                }
                if (nextNode == null) {
                    Debug.LogError("could not find ResultNode that matches skill check result");
                }
                return nextNode;
            }

            public int GetPlayerAttribute(AttributeType type) {
                return player.GetAttribute(type);
            }

        }

    }
}
