﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.UI;

namespace Barolo {

	using Events;

	namespace Encounter.MinorEncounter.UI {

		public class Choice : MonoBehaviour {

			protected Button button;
			
			[HideInInspector] public string localizationKey;

			void OnEnable() {
				button = GetComponent<Button>();
				button.onClick.AddListener(ChoiceSelected);
			}

			void ChoiceSelected() {
				ChoiceSelectedEventArgs args = new ChoiceSelectedEventArgs(localizationKey);
				EventManager.AddToQueue(EventName.CHOICE_SELECTED, args);
			}

		}
	}
}
