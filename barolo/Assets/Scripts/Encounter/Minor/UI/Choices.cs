﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Events;
	using UI;

	namespace Encounter.MinorEncounter.UI {

		public class Choices : MonoBehaviour {

			public GameObject choicePrefab;

			private GameObject _choicePrefabBackup;

			void Awake() {
				_choicePrefabBackup = choicePrefab;
			}

			void OnEnable() {
				EventManager.StartListening(EventName.CHOICES_ENCOUNTERED, OnChoicesEncountered);
			}

			void OnDisable() {
				EventManager.StopListening(EventName.CHOICES_ENCOUNTERED, OnChoicesEncountered);
			}

			void OnChoicesEncountered(GameEventArgs args) {
				this.gameObject.SetActive(true);
				ChoicesEventArgs choicesArgs = (ChoicesEventArgs)args;
				foreach (string choiceKey in choicesArgs.choiceKeys) {
					GameObject choiceObject = Instantiate(_choicePrefabBackup, this.gameObject.transform);

					LocalizedText localizedText = choiceObject.GetComponentInChildren<LocalizedText>();
					localizedText.SetKey(choiceKey);

					Choice choiceComponent = choiceObject.GetComponent<Choice>();
					choiceComponent.localizationKey = choiceKey;

					_choicePrefabBackup = choicePrefab;
				}
			}

		}

	}
}
