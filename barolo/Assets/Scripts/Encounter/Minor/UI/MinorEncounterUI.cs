﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.UI;

namespace Barolo {

	using Events;
	using UI;
	using Cards;
	using Encounter.UI;

	namespace Encounter.MinorEncounter.UI {

		public class MinorEncounterUI : MonoBehaviour {

			public Choices choices;
			public DialogueBox dialogue;
			public Hand hand;

			void OnEnable() {
				EventManager.StartListening(EventName.NODE_ADVANCED, OnNodeAdvanced);
				EventManager.StartListening(EventName.SKILL_CHECK_ENCOUNTERED, OnSkillCheckEncountered);
				EventManager.StartListening(EventName.SKILL_CHECK_COMPLETE, OnSkillCheckComplete);
				EventManager.StartListening(EventName.DIALOGUE_ENCOUNTERED, OnDialogueEncountered);
				EventManager.StartListening(EventName.CHOICE_SELECTED, OnChoiceSelected);
			}

			void OnDisable() {
				EventManager.StopListening(EventName.SKILL_CHECK_ENCOUNTERED, OnSkillCheckEncountered);
				EventManager.StopListening(EventName.SKILL_CHECK_COMPLETE, OnSkillCheckComplete);
				EventManager.StopListening(EventName.DIALOGUE_ENCOUNTERED, OnDialogueEncountered);
				EventManager.StopListening(EventName.CHOICE_SELECTED, OnChoiceSelected);
			}

			void OnNodeAdvanced(GameEventArgs args) {
				hand.gameObject.SetActive(false);
				choices.gameObject.SetActive(false);
				dialogue.gameObject.SetActive(false);
			}

			void OnDialogueEncountered(GameEventArgs args) {
				dialogue.gameObject.SetActive(true);
			}

			void OnChoiceSelected(GameEventArgs args) {
				foreach (Transform choice in choices.transform) {
					Destroy(choice.gameObject);
				}
				choices.gameObject.SetActive(false);
			}

			void OnSkillCheckEncountered(GameEventArgs args) {
				hand.gameObject.SetActive(true);
			}

			void OnSkillCheckComplete(GameEventArgs args) {}
		}

	}
}
