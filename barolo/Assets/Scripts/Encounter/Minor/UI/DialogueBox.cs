﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Events;

	namespace UI {

		public class DialogueBox : MonoBehaviour {

			protected LocalizedText localizedText;

			void Awake() {
				this.localizedText = GetComponentInChildren<LocalizedText>();
			}

			void OnEnable() {
				EventManager.StartListening(EventName.DIALOGUE_ENCOUNTERED, OnDialogueEncountered);
			}

			void OnDisable() {
				EventManager.StopListening(EventName.DIALOGUE_ENCOUNTERED, OnDialogueEncountered);
			}

			void OnDialogueEncountered(GameEventArgs args) {
				DialogueEventArgs dialogueArgs = (DialogueEventArgs)args;
				localizedText.SetKey(dialogueArgs.line);
			}

		}

	}
}
