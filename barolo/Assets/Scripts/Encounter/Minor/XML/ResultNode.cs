﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System.Xml.Serialization;
using UnityEngine;

namespace Barolo {

	namespace Encounter.MinorEncounter {

		[XmlType("Result")]
		public class ResultNode : MinorEncounterNode {

			[XmlAttribute("min")]
			public int min;

			[XmlAttribute("max")]
			public int max;
		}

	}
}
