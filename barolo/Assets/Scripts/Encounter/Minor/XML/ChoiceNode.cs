﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System.Xml.Serialization;

namespace Barolo {

	namespace Encounter.MinorEncounter {

		[XmlType("Choice")]
		public class ChoiceNode : MinorEncounterNode {

			[XmlAttribute("key")]
			public string key;
		}

	}
}
