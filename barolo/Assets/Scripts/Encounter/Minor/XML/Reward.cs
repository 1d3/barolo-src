﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System;
using System.Xml.Serialization;
using UnityEngine;

namespace Barolo {

	namespace Encounter.MinorEncounter {

		[XmlInclude(typeof(CardReward))]
		public class Reward {
			
			[XmlAttribute("id")]
			public int id;

		}

		public class CardReward : Reward {}

	}
}
