﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System.Xml.Serialization;
using UnityEngine;

namespace Barolo {
	using Events;

	namespace Encounter.MinorEncounter {

		[XmlType("Check")]
		public class CheckNode : MinorEncounterNode {

			[XmlAttribute("attribute")]
			public AttributeType attribute;

			[XmlArray(ElementName="Results")]
			[XmlArrayItem(ElementName="Result", Type=typeof(ResultNode))]
			public ResultNode[] results;

			public override void AfterDialogueEncountered() {
				Player player = GameObject.FindGameObjectWithTag(Constants.Tags.PLAYER_TAG).GetComponent<Player>();
				player.DrawCards(Constants.Game.MINOR_ENCOUNTER_DRAW_NUM);

				int attributeValue = MinorEncounterManager.Instance.GetPlayerAttribute(attribute);
				SkillCheckCompleteEventArgs skillArgs = new SkillCheckCompleteEventArgs(attributeValue);
				EventManager.AddToQueue(EventName.SKILL_CHECK_COMPLETE, skillArgs);
			}

		}

	}
}
