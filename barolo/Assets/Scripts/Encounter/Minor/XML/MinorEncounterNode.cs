﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System;
using System.Xml.Serialization;
using UnityEngine;

namespace Barolo {

    using Events;

    namespace Encounter.MinorEncounter {

        [XmlInclude(typeof(ChoiceNode))]
        [XmlInclude(typeof(CheckNode))]
        public class MinorEncounterNode {

            [XmlElement(ElementName="Dialogue", IsNullable=true)]
            public string dialogue;

            [XmlArray(ElementName="Choices", IsNullable=true)]
            [XmlArrayItem(ElementName="Choice", IsNullable=true, Type=typeof(ChoiceNode))]
            public ChoiceNode[] choices;

            [XmlElement("Check", IsNullable=true, Type=typeof(CheckNode))]
            public CheckNode checkNode;

            [XmlArray(ElementName="Rewards", IsNullable=true)]
            [XmlArrayItem(ElementName="Card", IsNullable=true, Type=typeof(CardReward))]
            public Reward[] rewards;

            public void Execute() {
                if (dialogue != null) {
                    DialogueEventArgs dialogeArgs = new DialogueEventArgs(dialogue);
                    EventManager.AddToQueue(EventName.DIALOGUE_ENCOUNTERED, dialogeArgs);
                }
                if (checkNode != null) {
                    SkillCheckEncounteredEventArgs skillArgs = new SkillCheckEncounteredEventArgs();
                    EventManager.AddToQueue(EventName.SKILL_CHECK_ENCOUNTERED, skillArgs);
                    return;
                }
                if (choices != null) {
                    string[] choiceKeys = GetChoiceKeys();
                    ChoicesEventArgs choicesArgs = new ChoicesEventArgs(choiceKeys);
                    EventManager.AddToQueue(EventName.CHOICES_ENCOUNTERED, choicesArgs);
                }
                AfterDialogueEncountered();
            }

            public string[] GetChoiceKeys() {
                int numChoices = choices.Length;
                string[] choiceKeys = new string[numChoices];
                for (int i = 0; i < numChoices; i++) {
                    choiceKeys[i] = choices[i].key;
                }
                return choiceKeys;
            }

            public virtual void AfterDialogueEncountered() {}
        }

    }
}
