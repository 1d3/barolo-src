﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System.Xml.Serialization;
using UnityEngine;

namespace Barolo {

    namespace Encounter.MinorEncounter {

        [XmlRoot("Encounter")]
        public class MinorEncounter : MinorEncounterNode {}

    }
}
