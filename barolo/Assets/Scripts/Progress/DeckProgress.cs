﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System.Collections.Generic;
using UnityEngine;

namespace Barolo {

	using Cards;
	using SaveLoad;

	namespace Progress {

		public class DeckProgress : MonoBehaviour {

			private DeckGlob deckGlob;

			void Awake() {
				SaveDeck(DebugCreateMockDeck());
			}

			public void SaveDeck(Deck deck) {
				deckGlob = DeckSaveLoad.ToGlob(deck);
				foreach (Card card in deck.GetCards()) {
					Destroy(card.gameObject);
				}
			}

			public Deck LoadDeck() {
				return DeckSaveLoad.FromGlob(deckGlob);
			}

			Deck DebugCreateMockDeck() {
				List<CardGlob> cardGlobs = new List<CardGlob>();
				string[] cards = new string[]{ "Human Gun", "Human Gun", "Human Gun", "Cannon", "Cannon", "Boop", "Boop", "Garbage Salvager", "Garbage Salvager" };
				foreach (string card in cards) {
					CardGlob glob = new CardGlob();
					glob.cardName = card;
					glob.assetBundle = Constants.Bundles.CardsBundle;
					cardGlobs.Add(glob);
				}

				DeckGlob deckGlob = new DeckGlob();
				deckGlob.cardGlob = cardGlobs;
				Deck deck = DeckSaveLoad.FromGlob(deckGlob);
				return deck;
			}
			
		}

	}
}
