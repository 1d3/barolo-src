﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEditor;
using System;
using System.IO;

using Barolo;

namespace BaroloEditor {

	public class LocalizedTextEditor : EditorWindow {

		public LocalizationData localizationData;

		[MenuItem("Window/Localized Text Editor")]
		static void Init() {
			EditorWindow.GetWindow(typeof(LocalizedTextEditor)).Show();
		}

		private void OnGUI() {
			if (localizationData != null) {
				SerializedObject serializedObject = new SerializedObject(this);
				SerializedProperty serializedProperty = serializedObject.FindProperty("localizationData");
				EditorGUILayout.PropertyField(serializedProperty, true);
				serializedObject.ApplyModifiedProperties();

				if (GUILayout.Button("Save data")) {
					SaveGameData();
				}
			}

			if (GUILayout.Button("Load data")) {
				LoadGameData();
			}

			if (GUILayout.Button("Create new data")) {
				CreateNewData();
			}
		}

		private void SaveGameData() {
			string filePath = EditorUtility.SaveFilePanel("Save localization data file", Constants.Files.LOCALIZATION_PATH, "", "json");

			if (!string.IsNullOrEmpty(filePath)) {
				Array.Sort(localizationData.items, delegate(LocalizationItem x, LocalizationItem y) { return x.key.CompareTo(y.key); });

				string dataAsJson = JsonUtility.ToJson(localizationData);
				File.WriteAllText(filePath, dataAsJson);
			}
		}

		private void LoadGameData() {
			string filePath = EditorUtility.OpenFilePanel("Select localization data file", Constants.Files.LOCALIZATION_PATH, "json");

			if (!string.IsNullOrEmpty(filePath)) {
				string dataAsJson = File.ReadAllText(filePath);
				localizationData = JsonUtility.FromJson<LocalizationData>(dataAsJson);
			}
		}

		private void CreateNewData() {
			localizationData = new LocalizationData();
		}

	}

}