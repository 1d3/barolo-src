﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

using Barolo.Events;

namespace SceneUtils {

	public class SceneLoader : MonoBehaviour {

		IEnumerator LoadSceneAsync(string sceneName, LoadSceneMode mode=LoadSceneMode.Additive) {
			yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);;
		}

		public void LoadScene(string sceneName, LoadSceneMode mode=LoadSceneMode.Additive) {
			StartCoroutine(LoadSceneAsync(sceneName, mode));
		}
	}

}
