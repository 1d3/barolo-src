﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.SceneManagement;

namespace SceneUtils {

	public class LoadThenMoveToNextScene : MonoBehaviour {

		public SceneField firstScene;
		public SceneLoader sceneLoader;

		void OnEnable() {
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		void OnDisable() {
			SceneManager.sceneLoaded -= OnSceneLoaded;
		}

		void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
			Debug.Log(SceneManager.GetActiveScene().name);
			sceneLoader.LoadScene(firstScene);
			Destroy(this.gameObject);
		}

	}

}
