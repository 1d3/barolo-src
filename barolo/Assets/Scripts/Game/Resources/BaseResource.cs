﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System;
using UnityEngine;
using UnityEngine.UI;

namespace Barolo {

	using Encounter.Resource.UI;

	namespace Encounter.Resource {

		public abstract class BaseResource : MonoBehaviour {

			public int current;
			public int max;
			public ResourceUI uIPrefab;
			public Transform uITransform;

			public void Init(int current, int max) {
				this.max = max;
				this.current = current;

				if (uIPrefab != null && uITransform != null && !uIPrefab.gameObject.activeInHierarchy) {
					uIPrefab = Instantiate(uIPrefab, uITransform);
				}

				if (uIPrefab != null) {
					uIPrefab.Init(current, max);
				}
			}

			public void Pay(int num) {
				current = current - num;
				current = Math.Min(current, max);
				if (uIPrefab != null) {
					uIPrefab.Refresh(Math.Max(current, 0), max);
				}
			}

			public void Add(int num) {
				Pay(-num);
			}

		}

	}
}
