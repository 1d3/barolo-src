﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using UnityEngine.UI;

namespace Barolo {
	namespace Encounter.Resource.UI {

		public class CounterResource : ResourceUI {

			public Text counter;

			public override void Init(int current, int max) {
				counter.text = current.ToString();
			}

			public override void Refresh(int current, int max) {
				counter.text = current.ToString();
			}

		}

	}
}
