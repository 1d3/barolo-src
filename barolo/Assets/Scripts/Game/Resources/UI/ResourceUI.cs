﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;
using UnityEngine.UI;

namespace Barolo {
	namespace Encounter.Resource.UI {

		public abstract class ResourceUI : MonoBehaviour {

			public abstract void Init(int current, int max);

			public abstract void Refresh(int current, int max);

		}

	}
}
