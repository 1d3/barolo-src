﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System;
using UnityEngine;
using UnityEngine.UI;

namespace Barolo {
	namespace Encounter.Resource.UI {

		public class SliderResource : ResourceUI {

			public Slider resourceSlider;
			public Text sliderText;

			public override void Init(int current, int max) {
				resourceSlider.maxValue = max;
				Refresh(current, max);
			}

			public override void Refresh(int current, int max) {
				resourceSlider.value = current;
				sliderText.text = String.Format("{0} / {1}", current, max);
			}

		}

	}
}
