﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System;
using System.Collections.Generic;

namespace Barolo {
	namespace Encounter.Resource {

		public static class ResourceManager {

			public static Dictionary<ResourceType, Type> resources;

			static ResourceManager() {
				resources = new Dictionary<ResourceType, Type>() {
					{ ResourceType.ACTION_POINTS, typeof(ActionPoints) },
					{ ResourceType.HEALTH, typeof(Health) },
					{ ResourceType.GARBAGE, typeof(Garbage) }
				};
			}

		}

	}
}
