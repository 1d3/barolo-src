﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System;
using UnityEngine;

namespace Barolo {
	namespace Encounter.Resource {

		using Encounter.Resource.UI;

		[Serializable]
		public class ResourceData {

			public ResourceType resourceType;
			public int starting;
			public int maximum;
			public ResourceUI uiPrefab;

		}

	}
}
