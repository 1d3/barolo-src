﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {
	namespace Encounter.MajorEncounter {

		public enum ReplacementEffectType {
			ON_DEATH,
			ON_DAMAGE_DEALT,
		}

	}
}
