﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

namespace Barolo {

	public enum FactionType {
		Human,
		Shapeshifter,
		Chaos
	}

}