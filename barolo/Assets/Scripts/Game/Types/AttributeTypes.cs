﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

namespace Barolo {

	public enum AttributeType {
		Athletics,
		Academics,
		Engineering,
		Reflexes,
		Willpower,
		Charisma,
		Perception
	}

}
