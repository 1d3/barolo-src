﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

namespace Barolo {

	public enum EnemyType {
		Minion,
		Boss
	}

}