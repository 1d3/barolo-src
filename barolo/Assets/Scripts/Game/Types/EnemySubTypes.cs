﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

namespace Barolo {

	public enum EnemySubType {
		Human,
		Mechanical
	}

}