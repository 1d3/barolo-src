﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

namespace Barolo {

	public enum DamageType {
		God,
		Physical,
		Mental,
		Subterfuge
	}

}
