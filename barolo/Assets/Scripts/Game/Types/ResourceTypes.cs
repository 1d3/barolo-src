﻿/*
 * Copyright (c) Too Many Bees, llc
 */

namespace Barolo {

	public enum ResourceType {
		HEALTH,
		ACTION_POINTS,
		GARBAGE
	}

}
