﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.UI;
using System;

namespace Barolo {
	namespace UI {

	public class LocalizedText : MonoBehaviour {

		public string folder;

		public string key;

		private Text _text;
		private LocalizationManager _localizationManager;

		void Awake() {
			_text = GetComponent<Text>();
			_localizationManager = GameObject.FindObjectOfType<LocalizationManager>();
			if (!String.IsNullOrEmpty(key)) {
				Refresh();
			}
		}

		public void SetKey(string value) {
			key = value;
			if (_localizationManager) {
				Refresh();
			}	
		}

		void Refresh() {
			if (folder != null && key != null) {
				_text.text = _localizationManager.GetLocalizedValue(key, folder);
			} else {
				Debug.LogError(String.Format("Attempting to localize text with invalid {0} folder or {1} key", folder, key));
			}
		}
	}

	}
}
