﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;

namespace Barolo {

	public class LocalizationManager : MonoBehaviour {

		public bool IsReady { get; private set; }

		private Dictionary<string, string> localizedText;
		private string currentFolder;

		void Awake() {
			IsReady = false;
		}
		
		public void LoadLocalizedText(string folder) {
			localizedText = new Dictionary<string, string>();
			string filePath = Path.Combine(Constants.Files.LOCALIZATION_PATH, Path.Combine(folder, "en.json"));
			if (File.Exists(filePath)) {
				string dataAsJson = File.ReadAllText(filePath);
				LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

				foreach (LocalizationItem item in loadedData.items) {
					localizedText.Add(item.key, item.value);
				}
				
			} else {
				Debug.LogError(String.Format("Localized file {0} not found", filePath));
			}

			IsReady = true;
		}

		public string GetLocalizedValue(string key, string folder) {
			if (folder != currentFolder) {
				LoadLocalizedText(folder);
				currentFolder = folder;
			}
			return localizedText.ContainsKey(key) ? localizedText[key] : "Localized value not found";
		}
	}

}