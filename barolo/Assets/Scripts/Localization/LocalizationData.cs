﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System;

namespace Barolo {

	[Serializable]
	public class LocalizationData {
		public LocalizationItem[] items;
	}

	[Serializable]
	public class LocalizationItem {
		public string key;
		public string value;
	}

}