﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Constants {

	public static class Tags {

		public static string PLAYER_TAG = "Player";

		public static string ENEMY_TAG = "Enemy";

		public static string CARD_TAG = "Card";

		public static string PROGRESS_TAG = "Progress";
	}

}