﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Constants {

	public static class Game {

		public static int MINOR_ENCOUNTER_DRAW_NUM = 1;

		public static int MAJOR_ENCOUNTER_START_DRAW_NUM = 2;

		public static int MAJOR_ENCOUNTER_TURN_DRAW_NUM = 1;

		public static int MAX_AFFINITY_LEVEL = 5;
	}

}