﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System.IO;

namespace Constants {

	public static class Files {


		public static string LOCALIZATION_PATH = Path.Combine(Application.streamingAssetsPath, "Localization");

		public static string ASSET_BUNDLES_PATH = Path.Combine("Assets", "AssetBundles");

		public static string DECK_GLOB_PATH = Path.Combine(Application.persistentDataPath, "deck.dat");

	}

}