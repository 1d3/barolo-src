﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Constants {

	public static class Bundles {

		public static string MinorEncountersBundle = "minorencounters";

		public static string EnemiesBundle = "enemies";

		public static string CardsBundle = "cards";

	}

}
