﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Barolo.Encounter.MinorEncounter;

	namespace SaveLoad.Bundles {

		public static class MinorEncounterLoader {

			public static MinorEncounter LoadMinorEncounter(string name, string bundleName) {
				AssetBundle bundle = AssetBundleManager.GetAssetBundle(bundleName);
				TextAsset encounterText = bundle.LoadAsset<TextAsset>(name);
				return XMLOp.DeserializeFromText<MinorEncounter>(encounterText.text);
			}

		}

	}
}
