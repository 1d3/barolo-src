﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Barolo {
	namespace SaveLoad.Bundles {

		public static class AssetBundleManager {

			private static Dictionary<string, AssetBundle> bundles;

			static AssetBundleManager() {
				bundles = new Dictionary<string, AssetBundle>();
			}

			public static AssetBundle GetAssetBundle(string key) {
				AssetBundle bundle;
				if (!bundles.TryGetValue(key, out bundle)) {
					bundle = BundleLoader.LoadBundle(key);
					bundles.Add(key, bundle);
				}
				return bundle;
			}

			public static IEnumerator LoadAssetBundle(string key) {
				if (bundles.ContainsKey(key)) {
					yield return null;
				} else {
					AssetBundleCreateRequest request = BundleLoader.RequestBundleAsync(key);
					yield return request;
					bundles.Add(key, request.assetBundle);
				}
			}

			public static void Unload(string key, bool unloadAllLoadedObjects) {
				AssetBundle bundle;
				if (bundles.TryGetValue(key, out bundle)) {
					bundle.Unload(unloadAllLoadedObjects);
					bundles.Remove(key);
				}
			}

			public static void Unload(AssetBundle bundle, bool unloadAllLoadedObjects) {
				bundle.Unload(unloadAllLoadedObjects);
				foreach (string key in bundles.Keys) {
					if (bundles[key] == bundle) {
						bundles.Remove(key);
						break;
					}
				}
			}

		}

	}
}
