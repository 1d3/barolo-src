﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Barolo {
	namespace SaveLoad.Bundles {

		public static class BundleLoader {

			public static AssetBundleCreateRequest RequestBundleAsync(string bundle) {
				return AssetBundle.LoadFromFileAsync(GetBundlePath(bundle));
			}

			public static AssetBundle LoadBundle(string bundle) {
				return AssetBundle.LoadFromFile(GetBundlePath(bundle));
			}

			public static AssetBundle GetBundleFromRequest(AssetBundleCreateRequest request) {
				AssetBundle bundle = request.assetBundle;
				if (bundle == null) {
					throw new Exception("Failed to load AssetBundle");
				}
				return bundle;
			}

			public static string GetSceneFromBundle(AssetBundle bundle, string sceneName) {
				string[] scenePaths = bundle.GetAllScenePaths();
				foreach (string scenePath in scenePaths) {
					if (scenePath.Contains(sceneName)) {
						return Path.GetFileNameWithoutExtension(scenePath);
					}
				}
				throw new Exception(String.Format("Could not find a scene named {0}", sceneName));
			}

			public static string GetSceneFromRequest(AssetBundleCreateRequest request, string sceneName) {
				AssetBundle bundle = GetBundleFromRequest(request);
				return GetSceneFromBundle(bundle, sceneName);
			}

			public static string GetBundlePath(string bundleName) {
				string platform;

				switch (Application.platform) {
					case RuntimePlatform.OSXEditor:
						platform = "OSX";
						break;
					case RuntimePlatform.WindowsEditor:
						platform = "Win";
						break;
					default:
						throw new Exception(String.Format("Unsupported platform {0}", Application.platform));
				}

				return Path.Combine(Path.Combine(Constants.Files.ASSET_BUNDLES_PATH, platform), bundleName);
			}

		}

	}
}
