﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System.IO;

namespace Barolo {

	using Cards;

	namespace SaveLoad.Bundles {

		public static class CardLoader {

			public static Card LoadCard(string cardName, string bundleName) {
				AssetBundle bundle = AssetBundleManager.GetAssetBundle(bundleName);
				GameObject cardObject = bundle.LoadAsset<GameObject>(cardName);
				Card card = GameObject.Instantiate(cardObject).GetComponent<Card>();
				card.name = cardObject.name;
				card.assetBundle = bundle.name;
				return card;
			}

		}

	}
}
