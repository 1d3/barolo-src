﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Barolo {

	using Encounter.Enemies;

	namespace SaveLoad.Bundles {

		public static class EnemyLoader {

			public static Enemy LoadEnemy(string enemyName, string bundleName) {
				AssetBundle bundle = AssetBundleManager.GetAssetBundle(bundleName);
				GameObject enemyObject = bundle.LoadAsset<GameObject>(enemyName);
				Enemy enemy = enemyObject.GetComponent<Enemy>();
				return enemy;
			}

		}

	}
}
