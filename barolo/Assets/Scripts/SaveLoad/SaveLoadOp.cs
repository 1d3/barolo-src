﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Barolo {
	namespace SaveLoad {

		public static class SaveLoadOp {

			public static void saveDataToDisk<T>(string savePath, T glob) where T : SaveGlob {
				BinaryFormatter bf = new BinaryFormatter();
				FileStream file = File.Create(savePath);
				bf.Serialize(file, glob);
				file.Close();
			}

			public static T loadDataFromDisk<T>(string savePath) where T : SaveGlob {
				if (File.Exists(savePath)) {
					BinaryFormatter bf = new BinaryFormatter();
					FileStream file = File.Open(savePath, FileMode.Open);
					T glob = (T)bf.Deserialize(file);
					file.Close();
					return glob;
				} else {
					Debug.LogError(String.Format("Trying to load data from {0} but file does not exist", savePath));
					return default(T);
				}
			}
		}

	}
}
