﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System.IO;

namespace Barolo {

	using SaveLoad.Bundles;
	using Cards;

	namespace SaveLoad {

		public static class CardSaveLoad {

			public static Card FromGlob(CardGlob glob) {
				Card card = CardLoader.LoadCard(glob.cardName, glob.assetBundle);
				card.damageMarked = glob.damageMarked;
				card.gameObject.name = card.name;
				return card;
			}

			public static CardGlob ToGlob(Card card) {
				CardGlob glob = new CardGlob();
				glob.cardName = card.name;
				glob.assetBundle = card.assetBundle;
				glob.damageMarked = card.damageMarked;
				return glob;
			}
		}

	}
}
