﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System.Collections.Generic;

namespace Barolo {

	using Cards;
	using SaveLoad.Bundles;

	namespace SaveLoad {

		public static class DeckSaveLoad {

			public static Deck FromGlob(DeckGlob glob) {
				Card[] decklist = new Card[glob.cardGlob.Count];

				for (int i = 0; i < glob.cardGlob.Count; i++) {
					CardGlob cardGlob = glob.cardGlob[i];
					decklist[i] = CardSaveLoad.FromGlob(cardGlob);
				}

				return new Deck(decklist);
			}

			public static DeckGlob ToGlob(Deck deck) {
				List<CardGlob> cardGlobs = new List<CardGlob>();
				foreach (Card card in deck.GetCards()) {
					cardGlobs.Add(CardSaveLoad.ToGlob(card));
				}
				DeckGlob glob = new DeckGlob();
				glob.cardGlob = cardGlobs;
				return glob;
			}

			public static Deck FromGame() {
				GameObject[] cardObjects = GameObject.FindGameObjectsWithTag(Constants.Tags.CARD_TAG);
				Card[] cards = new Card[cardObjects.Length];
				for (int i = 0; i < cardObjects.Length; i++) {
					cards[i] = cardObjects[i].GetComponent<Card>();
				}
				Deck deck = new Deck(cards);
				return deck;
			}
		}

	}
}
