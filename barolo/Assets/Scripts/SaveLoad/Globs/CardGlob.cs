﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;

namespace Barolo {
	namespace SaveLoad {

		[Serializable]
		public class CardGlob : SaveGlob {
			public string cardName;
			public string assetBundle;
			public int damageMarked;
		}

	}
}