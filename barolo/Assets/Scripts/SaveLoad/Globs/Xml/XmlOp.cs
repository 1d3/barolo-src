﻿using System.IO;
using System.Xml.Serialization;

namespace Barolo {
	namespace SaveLoad {

		public static class XMLOp {
			public static void Serialize(object item, string path) {
				XmlSerializer serializer = new XmlSerializer(item.GetType());
				StreamWriter writer = new StreamWriter(path);
				serializer.Serialize(writer.BaseStream, item);
				writer.Close();
			}
		
			public static T Deserialize<T>(string path) {
				XmlSerializer serializer = new XmlSerializer(typeof(T));
				StreamReader reader = new StreamReader(path);
				T deserialized = (T)serializer.Deserialize(reader.BaseStream);
				reader.Close();
				return deserialized;
			}

			public static T DeserializeFromText<T>(string text) {
				XmlSerializer serializer = new XmlSerializer(typeof(T));
				T deserialized = (T)serializer.Deserialize(new StringReader(text));
				return deserialized;
			}
		}
	}
}
