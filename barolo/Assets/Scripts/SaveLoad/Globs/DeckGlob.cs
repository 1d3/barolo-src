﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System;
using System.Collections.Generic;

namespace Barolo {
	namespace SaveLoad {

		[Serializable]
		public class DeckGlob : SaveGlob {
			public List<CardGlob> cardGlob = new List<CardGlob>();
		}

	}
}
