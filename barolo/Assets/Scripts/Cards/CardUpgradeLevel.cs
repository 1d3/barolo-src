﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;

namespace Barolo {

	using Abilities;
	using Encounter.MajorEncounter;

	namespace Cards {

		[Serializable]
		public class CardUpgradeLevel {
			public int minimum;
			public int maximum;
			public CardUpgradeData data;

			protected bool _active;
			public bool Active {
				get { return _active; }
				set { SetActive(value); }
			}

			public ActivatedAbility GetAbility(Card card) {
				return data.GetAbility(card);
			}

			protected void SetActive(bool active) {
				if (active != Active) {
					_active = active;
					foreach (TriggeredAbility triggeredAbility in data.triggeredAbilities) {
						triggeredAbility.SetListening(active);
					}
				}
			}
		}

	}
}
