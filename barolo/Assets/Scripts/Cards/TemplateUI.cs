﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System;
using UnityEngine;
using UnityEngine.UI;

namespace Barolo {

	using UI;
	using Encounter.Resource;

	namespace Cards.UI {

		public class TemplateUI : MonoBehaviour {

			public LocalizedText cardName;
			public Transform resourceTransform;

			public void AddResource(ResourceData data) {
				BaseResource resource = this.gameObject.AddComponent(ResourceManager.resources[data.resourceType]) as BaseResource;
				resource.uIPrefab = data.uiPrefab;
				resource.uITransform = resourceTransform;
				resource.Init(data.starting, data.maximum);
			}

			public void RefreshResource(ResourceData data) {
				BaseResource resource = GetComponent(ResourceManager.resources[data.resourceType]) as BaseResource;
				resource.Init(data.maximum, data.maximum);
			}

			public void Refresh(CardUpgradeLevel cardUpgradeLevel) {
				cardName.SetKey(cardUpgradeLevel.data.displayName);
			}

		}

	}
}
