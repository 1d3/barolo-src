﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System;
using UnityEngine;

namespace Barolo {
	namespace Cards {

		[System.Serializable]
		public class AttributeBonus {

			public AttributeType type;

			public int value;

			public override string ToString() {
				string sign = value > 0 ? "plus" : "minus";

				return String.Format("{0}{1} {2}", sign, value, type);
			}

		}

	}
}
