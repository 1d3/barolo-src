﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.UI;
using System;

using Constants;

namespace Barolo {

	using Events;
	using Encounter.MajorEncounter;
	using Encounter.Resource;
	using Abilities;
	using Cards.UI;

	namespace Cards {

		public class Card : MonoBehaviour, IInteractableGameActor {

			public CardData data;
			public TemplateUI template;
			public ZoneType CurrentZone { get; set; }

			public GameObject ActorGameObject {
				get { return template.isActiveAndEnabled ? template.gameObject : this.gameObject; }
				set { }
			}

			protected int activeLevel = 0;
			[HideInInspector] public int damageMarked = 0;
			[HideInInspector] public string assetBundle;

			public CardUpgradeLevel GetLevelFromStats(int level) {
				if (data == null) {
					Debug.LogError("Attempting to access non-existent card data for card: " + this.name);
					return null;
				}
				return data.GetUpgradeLevel(level);
			}

			public CardUpgradeLevel GetActiveLevel() {
				return GetLevelFromStats(activeLevel);
			}

			public void ActivateLevelFromStats(int level) {
				CardUpgradeLevel oldLevel = GetLevelFromStats(activeLevel);
				CardUpgradeLevel newLevel = GetLevelFromStats(level);

				oldLevel.Active = false;
				newLevel.Active = true;

				foreach (TriggeredAbility ability in oldLevel.data.triggeredAbilities) {
					TriggeredAbilityHandler.RemoveTrigger(this, ability);
				}

				foreach(TriggeredAbility ability in newLevel.data.triggeredAbilities) {
					TriggeredAbilityHandler.RegisterTrigger(this, ability);
				}

				activeLevel = level;
			}

			public void InitResources() {
				CardUpgradeLevel activeUpgradeLevel = GetActiveLevel();
				foreach (ResourceData resourceData in activeUpgradeLevel.data.resourceData) {
					template.AddResource(resourceData);
				}
			}

			public void AwakeFromDeck(Transform destination) {
				this.template = Instantiate(this.template, destination);
				this.transform.SetParent(template.transform);
				this.template.name = this.gameObject.name;
				this.template.GetComponent<InteractableGameObject>().owner = this;
				InitResources();
				DealDamage(null, DamageType.God, this.damageMarked);
			}

			public void ClickOn() {
				BeginActivateAbility();
			}

			public void BeginActivateAbility() {
				ActivatedAbility abilityBeingActivated = GetActiveLevel().GetAbility(this);
				if (abilityBeingActivated != null) {
					ActivateAbilityEventArgs args = new ActivateAbilityEventArgs(this, abilityBeingActivated);
					EventManager.AddToQueue(EventName.ACTIVATE_ABILITY, args);
				}
			}

			public void ChangeZone(ZoneType destination) {
				this.CurrentZone = destination;
			}

			public void DealDamage(IInteractableGameActor source, DamageType type, int amount) {
				Health health = GetResource<Health>();
				if (health != null) {
					ChangeResource<Health>(source, amount);
					if (health.current <= 0 && activeLevel > 0) {
						int remainder = health.current;
						CardUpgradeLevel newLevel = LowerUpgradeLevel();
						foreach (ResourceData resourceData in newLevel.data.resourceData) {
							if (resourceData.resourceType == ResourceType.HEALTH) {
								template.RefreshResource(resourceData);
								break;
							}
						}
						template.Refresh(newLevel);
						int damageToThisLevel = amount + remainder;
						damageMarked += damageToThisLevel;
						DamageDealtEventArgs damageArgs = new DamageDealtEventArgs(amount, type, source, this, remainder);
						EventManager.AddToQueue(EventName.DAMAGE_DEALT, damageArgs);
						if (remainder < 0) {
							DealDamage(source, type, -remainder);
						}
					}
				}
			}

			public CardUpgradeLevel LowerUpgradeLevel() {
				CardUpgradeLevel currentLevel = GetActiveLevel();
				if (currentLevel.minimum == 0) {
					return currentLevel;
				}

				ActivateLevelFromStats(currentLevel.minimum-1);
				return GetActiveLevel();
			}

			public T GetResource<T>() where T : BaseResource {
				return this.ActorGameObject.GetComponent<T>();
			}

			public void ChangeResource<T>(IInteractableGameActor source, int amount) where T : BaseResource {
				BaseResource resource = this.ActorGameObject.GetComponent<T>();
				if (resource != null) {
					resource.Pay(amount);
				}
			}

		}

	}
}
