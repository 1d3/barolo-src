﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;

namespace Barolo {

	using Abilities;
	using Encounter.Resource;

	namespace Cards {

		[CreateAssetMenu(menuName="Cards/Upgrade Data")]
		public class CardUpgradeData : ScriptableObject {

			public string displayName;

			public ResourceData[] resourceData = new ResourceData[0];

			public ActivatedAbility[] activatedAbilities = new ActivatedAbility[0];

			public AttributeBonus[] attributeBonuses = new AttributeBonus[0];

			public TriggeredAbility[] triggeredAbilities = new TriggeredAbility[0];

			public ActivatedAbility GetAbility(IInteractableGameActor source) {
				foreach (ActivatedAbility ability in activatedAbilities) {
					if (ability.CanAct(source)) {
						return ability;
					}
				}
				return null;
			}

		}

	}
}
