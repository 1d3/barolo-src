﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace Cards {

		[CreateAssetMenu(menuName="Cards/Artifact Card Data")]
		public class ArtifactCardData : CardData {

			void OnValidate() {
				cardType = CardType.ARTIFACT;
			}

		}

	}
}
