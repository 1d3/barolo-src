﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;

namespace Barolo {
	namespace Cards {

		[CreateAssetMenu(menuName="Cards/Action Card Data")]
		public class ActionCardData : CardData {

			void OnValidate() {
				cardType = CardType.ACTION;
			}

		}

	}
}
