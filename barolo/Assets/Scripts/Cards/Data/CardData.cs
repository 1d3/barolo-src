﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using System;
using System.Collections.Generic;

using Constants;

namespace Barolo {
	namespace Cards {

		public abstract class CardData : ScriptableObject {

			public FactionType faction;
			
			public CardUpgradeLevel[] upgradeLevels = new CardUpgradeLevel[1];

			[HideInInspector] public CardType cardType;

			public CardUpgradeLevel GetUpgradeLevel(int attribute) {
				foreach (CardUpgradeLevel level in this.upgradeLevels) {
					if (attribute >= level.minimum && attribute <= level.maximum) {
						return level;
					}
				}
				throw new Exception(String.Format("Can't find upgrade level for a {0} {1} card", cardType, faction));
			}

		}

	}
}
