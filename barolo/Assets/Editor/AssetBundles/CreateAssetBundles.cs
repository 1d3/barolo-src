﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System.IO;
using UnityEngine;
using UnityEditor;

namespace BaroloEditor {

	public class CreateAssetBundles {

		static void BuildAssetBundles(string subfolder, BuildTarget target) {
			string assetBundleDirectory = Path.Combine(Constants.Files.ASSET_BUNDLES_PATH, subfolder);
			if (!Directory.Exists(assetBundleDirectory)) {
				Directory.CreateDirectory(assetBundleDirectory);
			}
			BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, target);
		}

		[MenuItem("Assets/Build AssetBundles/OSX")]
		static void BuildOSXAssetBundles() {
			BuildAssetBundles("OSX", BuildTarget.StandaloneOSX);
		}

		[MenuItem("Assets/Build AssetBundles/Win")]
		static void BuildWINAssetBundles() {
			BuildAssetBundles("WIN", BuildTarget.StandaloneWindows);
		}

	}

}
