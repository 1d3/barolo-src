﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

using Barolo.Encounter.Enemies;

namespace Test {
	namespace Enemies {

		public class TestSimpleEnemy : SimpleEnemy {

			protected override void HandleDeath() {}

		}

	}
}
