﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.IO;

using Barolo;
using Barolo.SaveLoad.Bundles;

namespace Test {

	[TestFixture]
	public class TestIntegrationMajorEncounter {

		Scene testScene;

		[OneTimeSetUp]
		public void OneTimeSetUp() {
			testScene = SceneManager.GetActiveScene();
		}

		[UnityTest]
		public IEnumerator TestIntegrationMajorEncounterLoads() {
			yield return SceneUtils.LoadBundleSceneAsync(Constants.Bundles.TestPreloadScene, Constants.Bundles.TestScenesBundle);
			
			yield return SceneManager.LoadSceneAsync("Major_Encounter", LoadSceneMode.Additive);

			SceneManager.SetActiveScene(SceneManager.GetSceneByName("Major_Encounter"));

			Assert.AreEqual("Major_Encounter", SceneManager.GetActiveScene().name);

			GameObject mem = GameObject.Find("GameManager");
			Assert.NotNull(mem);

			SceneManager.SetActiveScene(testScene);
			yield return SceneManager.UnloadSceneAsync(Constants.Bundles.TestPreloadScene);
			yield return SceneManager.UnloadSceneAsync("Major_Encounter");
		}
	}
}
