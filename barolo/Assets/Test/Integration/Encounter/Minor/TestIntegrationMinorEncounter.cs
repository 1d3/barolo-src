﻿using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;
using NUnit.Framework;
using System.Collections;

using Barolo;
using Barolo.Encounter.MinorEncounter;
using Barolo.Encounter.MinorEncounter.UI;
using Barolo.SaveLoad.Bundles;

namespace Test {

	[TestFixture]
	public class TestIntegrationMinorEncounter {

		Scene testScene;

		[OneTimeSetUp]
		public void OneTimeSetUp() {
			testScene = SceneManager.GetActiveScene();
		}

		[UnityTest]
		public IEnumerator TestMinorEncounterInitializes() {
			yield return SceneUtils.LoadBundleSceneAsync(Constants.Bundles.TestPreloadScene, Constants.Bundles.TestScenesBundle);
			yield return SceneManager.LoadSceneAsync("Minor_Encounter", LoadSceneMode.Additive);
			SceneManager.SetActiveScene(SceneManager.GetSceneByName("Minor_Encounter"));

			MinorEncounterUI ui = GameObject.FindObjectOfType<MinorEncounterUI>();

			Assert.NotNull(ui);

			SceneManager.SetActiveScene(testScene);
			yield return SceneManager.UnloadSceneAsync(Constants.Bundles.TestPreloadScene);
			yield return SceneManager.UnloadSceneAsync("Minor_Encounter");
		}
	}
}
