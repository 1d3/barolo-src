﻿
/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

using Barolo;
using Barolo.Events;
using Barolo.Cards;
using Barolo.Encounter.Resource;
using Barolo.Encounter.MajorEncounter;
using Barolo.SaveLoad.Bundles;

namespace Test {

	[TestFixture]
	public class TestCard {

		[Test]
		public void TestActivateUpgradeLevel() {
			Card upgradeCard = CardLoader.LoadCard("ArtifactWithUpgrades", Constants.Bundles.TestCardsBundle);
			upgradeCard.ActivateLevelFromStats(1);

			Assert.False(upgradeCard.GetLevelFromStats(0).Active);
			Assert.True(upgradeCard.GetLevelFromStats(1).Active);
			Assert.False(upgradeCard.GetLevelFromStats(2).Active);
		}

		[Test]
		public void TestActivateComplexUpgradeLevel() {
			Card upgradeCard = CardLoader.LoadCard("ArtifactWithComplexUpgrades", Constants.Bundles.TestCardsBundle);
			upgradeCard.ActivateLevelFromStats(2);

			Assert.False(upgradeCard.GetLevelFromStats(0).Active);
			Assert.True(upgradeCard.GetLevelFromStats(1).Active);
			Assert.True(upgradeCard.GetLevelFromStats(2).Active);
			Assert.False(upgradeCard.GetLevelFromStats(3).Active);
		}

		[Test]
		public void TestCardHasHealth() {
			Card healthCard = CardLoader.LoadCard("CardWithResource", Constants.Bundles.TestCardsBundle);
			healthCard.ActivateLevelFromStats(2);
			healthCard.AwakeFromDeck(healthCard.transform.parent);
			Health health = healthCard.GetResource<Health>();

			Assert.NotNull(health);
			Assert.AreEqual(5, health.current);
			Assert.AreEqual(5, health.max);
		}

		[Test]
		public void TestCardPayResource() {
			Card healthCard = CardLoader.LoadCard("CardWithResource", Constants.Bundles.TestCardsBundle);
			healthCard.ActivateLevelFromStats(2);
			healthCard.AwakeFromDeck(healthCard.transform.parent);
			healthCard.ChangeResource<Health>(null, 1);
			Health health = healthCard.GetResource<Health>();

			Assert.NotNull(health);
			Assert.AreEqual(4, health.current);
			Assert.AreEqual(5, health.max);
		}

		[Test]
		public void TestLowerUpgradeLevel() {
			Card upgradeCard = CardLoader.LoadCard("ArtifactWithComplexUpgrades", Constants.Bundles.TestCardsBundle);
			upgradeCard.ActivateLevelFromStats(4);
			Assert.AreEqual("StrongUpgrade", upgradeCard.GetActiveLevel().data.displayName);
			upgradeCard.LowerUpgradeLevel();
			Assert.AreEqual("WeakUpgrade", upgradeCard.GetActiveLevel().data.displayName);
		}

		[Test]
		[TestCase(1, 4, "HasMoreHealth")]
		[TestCase(5, 5, "HasHealth")]
		[TestCase(6, 4, "HasHealth")]
		[TestCase(10, 0, "Rubble")]
		public void TestDamageRevertsAffinity(int damage, int remaining, string displayName) {
			Card healthCard = CardLoader.LoadCard("CardWithResource", Constants.Bundles.TestCardsBundle);
			healthCard.ActivateLevelFromStats(5);
			healthCard.AwakeFromDeck(healthCard.transform.parent);
			healthCard.DealDamage(null, DamageType.Physical, damage);

			Health health = healthCard.GetResource<Health>();
			Assert.AreEqual(remaining, health.current);
			CardUpgradeLevel newLevel = healthCard.GetActiveLevel();
			Assert.AreEqual(displayName, newLevel.data.displayName);
		}

		[Test]
		[TestCase(1, 4, "HasMoreHealth")]
		[TestCase(5, 5, "HasHealth")]
		[TestCase(6, 4, "HasHealth")]
		[TestCase(10, 0, "Rubble")]
		public void TestCardUsesDamageMarkedOnAwake(int damage, int remaining, string displayName) {
			Card healthCard = CardLoader.LoadCard("CardWithResource", Constants.Bundles.TestCardsBundle);
			healthCard.ActivateLevelFromStats(5);
			healthCard.damageMarked = damage;
			healthCard.AwakeFromDeck(healthCard.transform.parent);

			Health health = healthCard.GetResource<Health>();
			Assert.AreEqual(remaining, health.current);
			CardUpgradeLevel newLevel = healthCard.GetActiveLevel();
			Assert.AreEqual(displayName, newLevel.data.displayName);
		}

		[Test]
		public void TestActivatingCardLevelRemovesOldTriggers() {
			TriggeredAbilityHandler.triggers.Clear();
			Card triggerCard = CardLoader.LoadCard("CardWithTrigger", Constants.Bundles.TestCardsBundle);
			triggerCard.ActivateLevelFromStats(1);
			Assert.AreEqual(1, TriggeredAbilityHandler.triggers.Count);
			triggerCard.ActivateLevelFromStats(0);
			Assert.AreEqual(0, TriggeredAbilityHandler.triggers.Count);
		}

	}

}
