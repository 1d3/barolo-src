﻿
/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

using Barolo;
using Barolo.SaveLoad;
using Barolo.SaveLoad.Bundles;
using Barolo.Cards;

namespace Test {

	[TestFixture]
	public class TestCardSaveLoad {

		[Test]
		public void TestCardGlobPropertiesSaveLoad() {
			string filepath = PathUtils.PersistentDataTestPath("card.dat");

			CardGlob savedCardGlob = new CardGlob();
			savedCardGlob.cardName = "testcard";
			savedCardGlob.assetBundle = "assetBundle";

			SaveLoadOp.saveDataToDisk<CardGlob>(filepath, savedCardGlob);
			CardGlob loadedCardGlob = SaveLoadOp.loadDataFromDisk<CardGlob>(filepath);

			Assert.AreEqual("testcard", loadedCardGlob.cardName);
			Assert.AreEqual("assetBundle", loadedCardGlob.assetBundle);

		}

		[Test]
		public void TestCardToGlob() {
			Card card = CardLoader.LoadCard("BasicCard", Constants.Bundles.TestCardsBundle);
			card.damageMarked = 1;
			CardGlob glob = CardSaveLoad.ToGlob(card);
			Assert.AreEqual("BasicCard", glob.cardName);
			Assert.AreEqual(Constants.Bundles.TestCardsBundle, glob.assetBundle);
			Assert.AreEqual(1, glob.damageMarked);
		}

		[Test]
		public void TestCardFromGlob() {
			CardGlob glob = new CardGlob();
			glob.cardName = "BasicCard";
			glob.assetBundle = Constants.Bundles.TestCardsBundle;
			glob.damageMarked = 1;
			Card card = CardSaveLoad.FromGlob(glob);
			Assert.AreEqual(glob.cardName, card.name);
			Assert.AreEqual(1, card.damageMarked);
		}
	}

}
