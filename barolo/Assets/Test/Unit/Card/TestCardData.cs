﻿
/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

using Barolo;
using Barolo.Cards;
using Barolo.SaveLoad.Bundles;

namespace Test {

	[TestFixture]
	public class TestCardData {

		[Test]
		public void TestBonusLoadsFromScriptableObject() {
			Card card = CardLoader.LoadCard("CardWithBonus", Constants.Bundles.TestCardsBundle);
			CardUpgradeLevel level = card.GetLevelFromStats(0);
			Assert.AreEqual(1, level.data.attributeBonuses.Length);

			AttributeBonus bonus = level.data.attributeBonuses[0];
			Assert.AreEqual(AttributeType.Athletics, bonus.type);
			Assert.AreEqual(1, bonus.value);

			GameObject.Destroy(card);
		}

		[Test]
		[TestCase("ArtifactWithUpgrades", 1, 2)]
		[TestCase("ArtifactWithComplexUpgrades", 2, 4)]
		public void TestAffinityLevelConvertsToCorrectUpgradeLevel(string name, int weakUpgradeLevel, int strongUpgradeLevel) {
			Card upgradeCard = CardLoader.LoadCard(name, Constants.Bundles.TestCardsBundle);
			CardUpgradeLevel weakUpgrade = upgradeCard.data.GetUpgradeLevel(weakUpgradeLevel);
			Assert.AreEqual("WeakUpgrade", weakUpgrade.data.name);
			CardUpgradeLevel strongUpgrade = upgradeCard.data.GetUpgradeLevel(strongUpgradeLevel);
			Assert.AreEqual("StrongUpgrade", strongUpgrade.data.name);

			GameObject.Destroy(upgradeCard);
		}
	}

}