﻿
/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

using Barolo;
using Barolo.SaveLoad.Bundles;
using Barolo.Events;
using Barolo.Encounter.MinorEncounter;

namespace Test {

	[TestFixture]
	public class TestMinorEncounter {

		[Test]
		public void TestCorrectChoiceKeys() {
			MinorEncounter encounter = MinorEncounterLoader.LoadMinorEncounter("SingleChoice", Constants.Bundles.TestMinorEncountersBundle);
			string[] choiceNames = encounter.GetChoiceKeys();

			Assert.AreEqual("ME_TEST_CHOICE_FIRST", choiceNames[0]);
			Assert.AreEqual("ME_TEST_CHOICE_SECOND", choiceNames[1]);
		}

		[Test]
		public void TestAdvanceToCorrectChoice() {
			MinorEncounter encounter = MinorEncounterLoader.LoadMinorEncounter("SingleChoice", Constants.Bundles.TestMinorEncountersBundle);
			
			ChoiceSelectedEventArgs keepWaklingArgs = new ChoiceSelectedEventArgs("ME_TEST_CHOICE_FIRST");
			MinorEncounterNode keepWalkingNode = MinorEncounterManager.ResolveChoiceSelected(encounter, keepWaklingArgs);
			Assert.AreEqual("ME_TEST_DIALOGUE_FIRST", keepWalkingNode.dialogue);

			ChoiceSelectedEventArgs climbArgs = new ChoiceSelectedEventArgs("ME_TEST_CHOICE_SECOND");
			MinorEncounterNode climbNode = MinorEncounterManager.ResolveChoiceSelected(encounter, climbArgs);
			Assert.AreEqual("ME_TEST_DIALOGUE_SECOND", climbNode.dialogue);
		}

		[Test]
		public void TestMovesToCorrectResult() {
			MinorEncounter encounter = MinorEncounterLoader.LoadMinorEncounter("SingleCheck", Constants.Bundles.TestMinorEncountersBundle);
			
			SkillCheckCompleteEventArgs positiveArgs = new SkillCheckCompleteEventArgs(5);
			MinorEncounterNode positiveNode = MinorEncounterManager.ResolveSkillCheck(encounter.checkNode, positiveArgs);
			Assert.AreEqual("ME_TEST_CHECK_PASS", positiveNode.dialogue);

			SkillCheckCompleteEventArgs negativeArgs = new SkillCheckCompleteEventArgs(-5);
			MinorEncounterNode negativeNode = MinorEncounterManager.ResolveSkillCheck(encounter.checkNode, negativeArgs);
			Assert.AreEqual("ME_TEST_CHECK_FAIL", negativeNode.dialogue);
		}
	}

}