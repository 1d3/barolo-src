﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

using Barolo;
using Barolo.Abilities;
using Barolo.Cards;
using Barolo.Encounter.MajorEncounter;
using Barolo.SaveLoad.Bundles;

namespace Test {

	[TestFixture]
	public class TestTriggeredAbilityHandler {

		[Test]
		public void TestAddTrigger() {
			TriggeredAbilityHandler.triggers.Clear();
			Card triggerCard = CardLoader.LoadCard("CardWithTrigger", Constants.Bundles.TestCardsBundle);
			triggerCard.ActivateLevelFromStats(1);
			Assert.AreEqual(1, TriggeredAbilityHandler.triggers.Count);
		}

		[Test]
		public void TestRemoveTrigger() {
			TriggeredAbilityHandler.triggers.Clear();
			Card triggerCard = CardLoader.LoadCard("CardWithTrigger", Constants.Bundles.TestCardsBundle);
			triggerCard.ActivateLevelFromStats(1);
			TriggeredAbility trigger = triggerCard.GetActiveLevel().data.triggeredAbilities[0];
			TriggeredAbilityHandler.RemoveTrigger(triggerCard, trigger);
			Assert.AreEqual(0, TriggeredAbilityHandler.triggers.Count);
		}
	}
}
