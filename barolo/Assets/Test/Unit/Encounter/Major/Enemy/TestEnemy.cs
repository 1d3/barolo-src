﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

using Barolo;
using Barolo.Events;
using Barolo.Encounter.Enemies;
using Barolo.SaveLoad.Bundles;
using Barolo.Encounter.Resource;

namespace Test {

	[TestFixture]
	public class TestEnemy {

		[Test]
		public void TestEnemyInitsHealth() {
			Enemy enemy = EnemyLoader.LoadEnemy("BasicEnemy", Constants.Bundles.TestEnemiesBundle);
			enemy.InitResources();
			Assert.AreEqual(enemy.data.maxHealth, enemy.GetResource<Health>().max);
			Assert.AreEqual(enemy.data.maxHealth, enemy.GetResource<Health>().current);
		}

		[Test]
		[TestCase("BasicEnemy", 1, 4)]
		[TestCase("BasicEnemy", 5, 0)]
		[TestCase("BasicEnemy", 10, -5)]
		public void TestDealtDamage(string enemyName, int amount, int healthRemaining) {
			Enemy enemy = EnemyLoader.LoadEnemy(enemyName, Constants.Bundles.TestEnemiesBundle);
			enemy.InitResources();
			enemy.DealDamage(null, DamageType.Physical, amount);
			Assert.AreEqual(healthRemaining, enemy.GetResource<Health>().current);
		}

		[Test]
		[TestCase("BasicEnemy", 1, false)]
		[TestCase("BasicEnemy", 5, true)]
		[TestCase("BasicEnemy", 10, true)]
		public void TestIsDefeated(string enemyName, int amount, bool defeated) {
			Enemy enemy = EnemyLoader.LoadEnemy(enemyName, Constants.Bundles.TestEnemiesBundle);
			enemy.InitResources();
			enemy.DealDamage(null, DamageType.Physical, amount);
			Assert.AreEqual(enemy.IsDefeated(), defeated);
		}
	}
}
