﻿
/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

using Barolo;
using Barolo.Encounter.Resource;
using Barolo.Events;
using Barolo.Cards;
using Barolo.SaveLoad.Bundles;

namespace Test {

	[TestFixture]
	public class TestPlayer {

		private Player player;

		[SetUp]
		public void SetUp() {
			player = new GameObject("Player", typeof(Player), typeof(Health), typeof(ActionPoints)).GetComponent<Player>();
		}

		[TearDown]
		public void TearDown() {
			GameObject.Destroy(player);
		}

		[Test]
		public void TestDrawCardsFromDeck() {
			string[] decklist = new string[3] { "BasicCard", "CardWithBonus", "ShapeshifterCard" };
			Deck deck = DeckUtils.SetUpDeck(decklist, Constants.Bundles.TestCardsBundle);
			player.SetUpDeck(deck);

			Card shapeshifterCard = player.DrawCard();
			Assert.AreEqual("ShapeshifterCard", shapeshifterCard.name);

			Card cardWithBonus = player.DrawCard();
			Assert.AreEqual("CardWithBonus", cardWithBonus.name);

			Card basicCard = player.DrawCard();
			Assert.AreEqual("BasicCard", basicCard.name);
		}

		[Test]
		public void TestAddCardsToHand() {
			string[] decklist = new string[3] { "BasicCard", "CardWithBonus", "ShapeshifterCard" };
			Deck deck = DeckUtils.SetUpDeck(decklist, Constants.Bundles.TestCardsBundle);
			player.SetUpDeck(deck);

			player.hand = new List<Card>();
			for (int i = 0; i < 3; i++) {
				Card card = player.DrawCard();
				player.AddCardToHand(card);
			}
			
			Assert.AreEqual(player.hand.Count, 3);
			Assert.AreEqual("ShapeshifterCard", player.hand[0].name);
			Assert.AreEqual("CardWithBonus", player.hand[1].name);
			Assert.AreEqual("BasicCard", player.hand[2].name);
		}
	}

}
