﻿
/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

using Barolo;
using Barolo.SaveLoad;
using Barolo.SaveLoad.Bundles;
using Barolo.Cards;

namespace Test {

	[TestFixture]
	public class TestDeckSaveLoad {

		[Test]
		public void TestDeckGlobPropertiesSaveLoad() {
			string filepath = PathUtils.PersistentDataTestPath("deck.dat");

			string[] decklist = new string[3] { "Card1", "Card2", "Card3" };
			List<CardGlob> savedCardGlob = new List<CardGlob>();
			for (int i = 0; i < decklist.Length; i++) {
				CardGlob cardGlob = new CardGlob();
				cardGlob.cardName = decklist[i];
				savedCardGlob.Add(cardGlob);
			}

			DeckGlob savedDeckGlob = new DeckGlob();
			savedDeckGlob.cardGlob = savedCardGlob;

			SaveLoadOp.saveDataToDisk<DeckGlob>(filepath, savedDeckGlob);
			DeckGlob loadedDeckGlob = SaveLoadOp.loadDataFromDisk<DeckGlob>(filepath);

			List<CardGlob> loadedCardGlob = loadedDeckGlob.cardGlob;

			Assert.AreEqual("Card1", loadedCardGlob[0].cardName);
			Assert.AreEqual("Card2", loadedCardGlob[1].cardName);
			Assert.AreEqual("Card3", loadedCardGlob[2].cardName);
			
		}

		[Test]
		public void TestDeckToGlob() {
			string[] decklist = new string[3] { "BasicCard", "CardWithBonus", "ShapeshifterCard" };
			Deck deck = DeckUtils.SetUpDeck(decklist, Constants.Bundles.TestCardsBundle);
			DeckGlob glob = DeckSaveLoad.ToGlob(deck);

			for (int i = 0; i < decklist.Length; i++) {
				Card deckCard = deck.DrawCard();
				CardGlob cardGlob = glob.cardGlob[i];
				Assert.AreEqual(deckCard.name, cardGlob.cardName);
			}
		}

		[Test]
		public void TestDeckFromGlob() {
			List<CardGlob> cardGlobs = new List<CardGlob>();
			string[] decklist = new string[3] { "BasicCard", "CardWithBonus", "ShapeshifterCard" };
			foreach (string cardName in decklist) {
				cardGlobs.Add(CardSaveLoad.ToGlob(CardLoader.LoadCard(cardName, Constants.Bundles.TestCardsBundle)));
			}
			
			DeckGlob deckGlob = new DeckGlob();
			deckGlob.cardGlob = cardGlobs;
			Deck deck = DeckSaveLoad.FromGlob(deckGlob);

			for (int i = 0; i < deckGlob.cardGlob.Count; i++) {

				// Cards are pushed into deck stack from left to right, so first card drawn will be last element in CardGlob
				Card deckCard = deck.DrawCard();
				CardGlob cardGlob = deckGlob.cardGlob[deckGlob.cardGlob.Count-1-i];

				Assert.AreEqual(cardGlob.cardName, deckCard.name);
			}
		}
	}

}
