﻿
/*
 * Copyright (c) Too Many Bees, llc
 */ 

using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;

using Barolo;
using Barolo.Cards;
using Barolo.SaveLoad.Bundles;

namespace Test {

	[TestFixture]
	public class TestDeck {

		private Deck deck;

		private string[] basicDeck = new string[3] { "BasicCard", "BasicCard", "BasicCard" };

		[Test]
		public void TestLibraryCount() {
			deck = DeckUtils.SetUpDeck(basicDeck, Constants.Bundles.TestCardsBundle);

			Assert.AreEqual(deck.libraryCount, 3);
		}

		[Test]
		public void TestDrawCard() {
			deck = DeckUtils.SetUpDeck(basicDeck, Constants.Bundles.TestCardsBundle);

			Card card = deck.DrawCard();
			Assert.AreEqual(deck.libraryCount, 2);
			Assert.NotNull(card);
		}

		[Test]
		public void TestDrawCardFromEmptyDeck() {
			deck = DeckUtils.SetUpDeck(new string[0]{}, Constants.Bundles.TestCardsBundle);

			Card card = deck.DrawCard();
			Assert.Null(card);
		}

		[Test]
		public void TestShuffle() {
			string[] decklist = new string[3] { "BasicCard", "CardWithBonus", "ShapeshifterCard" };
			deck = DeckUtils.SetUpDeck(decklist, Constants.Bundles.TestCardsBundle);

			// set up the names of the cards we expect to see in the deck
			List<string> names = new List<string>();
			Card card = deck.DrawCard();
			while (card != null) {
				names.Add(card.name);
				card = deck.DrawCard();
			}

			if (deck.libraryCount > 0) {
				Assert.Fail("There was a null card in the deck");
			}

			// shuffle the deck, then verify those names are still there
			deck = DeckUtils.SetUpDeck(decklist, Constants.Bundles.TestCardsBundle);
			deck.Shuffle();

			card = deck.DrawCard();
			while (card != null) {
				bool found = false;
				int foundIndex = -1;
				for (int i = 0; i < names.Count; i++) {
					if (names[i] == card.GetComponent<Card>().name) {
						found = true;
						foundIndex = i;
						break;
					}
				}
				if (!found) {
					Assert.Fail(String.Format("Card with name: {0} not found in shuffled deck."));
				}
				names.RemoveAt(foundIndex);
				card = deck.DrawCard();
			}
		}
	}

}