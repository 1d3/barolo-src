﻿/*
 * Copyright (c) Too Many Bees, llc
 */ 

using System.IO;
using UnityEngine;

namespace Test {
	
	namespace Constants {

		public static class Files {

			public static string TEST_PATH = "Test";

			public static string ASSET_BUNDLES_PATH = Path.Combine("Assets", "AssetBundles");

			public static string DATA_PATH = Path.Combine(Application.dataPath, Path.Combine(TEST_PATH, "Data"));

			public static string PERSISTENT_TEST_PATH = Path.Combine(DATA_PATH, "PersistentData");


		}

	}

}