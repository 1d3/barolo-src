﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

namespace Test {
	namespace Constants {

		public static class Bundles {

			public static string TestScenesBundle = "testscenes";
			
			public static string TestCardsBundle = "testcards";

			public static string TestEnemiesBundle = "testenemies";

			public static string TestMinorEncountersBundle = "testminorencounters";

			public static string TestPreloadScene = "_test_preload";

		}

	}
}
