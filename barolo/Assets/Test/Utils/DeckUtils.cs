﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

using Barolo.Cards;
using Barolo.SaveLoad.Bundles;

namespace Test {

	public static class DeckUtils {

		public static Deck SetUpDeck(string[] cardNames, string bundleName) {
			return new Deck(CardUtils.LoadCardListFromBundle(cardNames, bundleName));
		}

	}

}
