﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System;
using System.IO;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

using Barolo.SaveLoad.Bundles;

namespace Test {

	public static class SceneUtils {

		public static IEnumerator LoadBundleSceneAsync(string sceneName, string bundleName, LoadSceneMode mode=LoadSceneMode.Additive) {
			yield return AssetBundleManager.LoadAssetBundle(bundleName);
			AssetBundle bundle = AssetBundleManager.GetAssetBundle(bundleName);
			string scene = GetSceneFromBundle(bundle, sceneName);
			yield return SceneManager.LoadSceneAsync(scene, mode);
		}

		public static string GetSceneFromBundle(AssetBundle bundle, string sceneName) {
			string[] scenePaths = bundle.GetAllScenePaths();
			foreach (string scenePath in scenePaths) {
				if (scenePath.Contains(sceneName)) {
					return Path.GetFileNameWithoutExtension(scenePath);
				}
			}
			throw new Exception(String.Format("Could not find a scene named {0}", sceneName));
		}

	}

}
