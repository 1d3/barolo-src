﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using System.IO;
using UnityEngine;

namespace Test {

	public static class PathUtils {

		public static string PersistentDataTestPath(string path) {
			return Path.Combine(Constants.Files.PERSISTENT_TEST_PATH, path);
		}

	}

}
