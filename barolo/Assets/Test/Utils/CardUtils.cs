﻿/*
 * Copyright (c) Too Many Bees, llc
 */

using UnityEngine;

using Barolo.Cards;
using Barolo.SaveLoad.Bundles;

namespace Test {

	public static class CardUtils {
		public static Card[] LoadCardListFromBundle(string[] cardNames, string bundleName) {
			Card[] cards = new Card[cardNames.Length];
			for (int i = 0; i < cardNames.Length; i++) {
				Card card = CardLoader.LoadCard(cardNames[i], bundleName);
				cards[i] = card;
			}
			return cards;
		}
	}

}
